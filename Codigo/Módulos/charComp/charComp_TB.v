`timescale 1ns / 1ps
module charComp_tb;
	reg [6:0] char;
	reg enter;
	wire cEnter, cEne, cVirgul, cDelete, cNum;
	
	charComp uut (.cEnter(cEnter), .cEne(cEne), .cNum(cNum), .cVirgul(cVirgul), .cDelete(cDelete), .char(char), .enter(enter));
	
	initial begin  
		char<=7'b0000000;
		enter=0;
	end	
     
	initial begin 
		forever begin		

		#150	
		char<=7'b0010000;
		
		#150
		char<=7'b0111001;
	
		#150	
		char<=7'b0111011;

		#150	
		char<=7'b0111111;

		#150	
		char<=7'b0001000;		

		#150	
		enter=1;	

		#150				
		char<=7'b0001001;		

		#150	
		char<=7'b0000110;

		#150	
		char<=7'b0110110;				
	end
	end 
	
	initial begin: TEST_CASE
		$dumpfile("charComp_TB.vcd");
		$dumpvars(-1, uut);
		#2000 $finish;
	end

endmodule
