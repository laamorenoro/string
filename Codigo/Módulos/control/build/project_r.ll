Revision 3
; Created by bitgen P.15xf at Wed Jun  5 17:17:17 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit  1083336 0x00280200     40 Block=P89 Latch=I Net=clk_BUFGP/IBUFG
Bit  2085959 0x02027000     71 Block=P78 Latch=O2 Net=complete_OBUF
Bit  2149213 0x04000000   1245 Block=SLICE_X67Y55 Latch=XQ Net=next_state_FSM_FFd2
Bit  2149338 0x04000000   1370 Block=SLICE_X67Y51 Latch=YQ Net=next_state_FSM_FFd3
Bit  2149405 0x04000000   1437 Block=SLICE_X67Y49 Latch=XQ Net=next_state_FSM_FFd1
Bit  2205104 0x04002400   1264 Block=P71 Latch=O2 Net=add_OBUF
Bit  2205127 0x04002400   1287 Block=P70 Latch=O2 Net=addEnhe_OBUF
Bit  2205895 0x04002400   2055 Block=P57 Latch=O2 Net=change_OBUF
Bit  2206832 0x04002400   2992 Block=P54 Latch=O2 Net=load_OBUF
Bit  2206855 0x04002400   3015 Block=P53 Latch=O2 Net=delete_OBUF
Bit  2211367 0x04020200   1319 Block=P69 Latch=I Net=cNum_IBUF
Bit  2211432 0x04020200   1384 Block=P68 Latch=I Net=cVirgul_IBUF
Bit  2211471 0x04020200   1423 Block=P67 Latch=I Net=new_IBUF
Bit  2211560 0x04020200   1512 Block=P66 Latch=I Net=cDelete_IBUF
Bit  2211599 0x04020200   1551 Block=P65 Latch=I Net=enter_IBUF
Bit  2211688 0x04020200   1640 Block=P63 Latch=I Net=cEnter_IBUF
Bit  2211727 0x04020200   1679 Block=P62 Latch=I Net=reset_IBUF
Bit  2211816 0x04020200   1768 Block=P61 Latch=I Net=virgul_IBUF
Bit  2211855 0x04020200   1807 Block=P60 Latch=I Net=cEne_IBUF
Bit  2212072 0x04020200   2024 Block=P58 Latch=I Net=cCounter_IBUF
