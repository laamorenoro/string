`timescale 1ns / 1ps
module control_tb;

	reg clk;
	reg cEnter;
	reg cVirgul;
	reg cEne;
	reg cDelete;
	reg cCounter;
	reg new;
	reg reset;
	reg cNum;
	wire complete;
	reg enter;
	wire delete;
	wire add;
	wire load;
	wire addEnhe;
	wire change;
	reg virgul;
	
	control uut ( .clk(clk), .cEnter(cEnter), .cVirgul(cVirgul), .cEne(cEne), .cDelete(cDelete), .cCounter(cCounter), .new(new), .reset(reset), .cNum(cNum), .complete(complete), .enter(enter), .delete(delete), .add(add), .load(load), .addEnhe(addEnhe), .change(change), .virgul(virgul));
	
	initial begin  // Initialize Inputs
		clk=0;
		cEnter=0;
		cVirgul=0;
		cEne=0;
		cDelete=0;
		cCounter=0;
		new=0;
		reset=0;
		cNum=0;
		reset=1;
		virgul=0;
		enter=0;
		#50
		reset=0;
	end	

     	initial begin 
		forever begin
		#50
		clk=1;
		#50
		clk=0;
		end
	end 

	initial begin 
		#100		
		#50
		cVirgul=1;
		virgul=1;
		#50
		cVirgul=0;
		cEne=1;
		#150		
		cVirgul=1;
		virgul=1;
		#200		
		new=1;
		cEne=0;
		cVirgul=0;
		#300
		cVirgul=1;
		virgul=1;
		#200
		cDelete=0;
		cVirgul=0;
		cCounter=1;
		#200
		cEnter=1;
		enter=1;
		#100
		cEnter=0;
		cNum=1;
		#200
		cNum=0;
		cDelete=1;

	end


	initial begin: TEST_CASE
		$dumpfile("control_TB.vcd");
		$dumpvars(-1, uut);
		#2000 $finish;
	end

endmodule
