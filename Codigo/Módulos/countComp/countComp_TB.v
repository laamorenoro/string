`timescale 1ns / 1ps
module countComp_tb;
	reg [3:0] counter;
	wire cCounter;
	
	countComp uut ( .cCounter(cCounter), .counter(counter));
	
	initial begin  // Initialize Inputs
		counter<=4'b0000;
	end	
     
	initial begin 
		forever begin		
		#50	
		counter<=counter+1'b1;		
		end
	end 
	
	initial begin: TEST_CASE
		$dumpfile("countComp_TB.vcd");
		$dumpvars(-1, uut);
		#2000 $finish;
	end

endmodule
