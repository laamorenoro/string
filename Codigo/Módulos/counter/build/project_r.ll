Revision 3
; Created by bitgen P.15xf at Wed Jun  5 17:31:51 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit    12656 0x00020000    240 Block=P5 Latch=O2 Net=Result<1>
Bit    12679 0x00020000    263 Block=P4 Latch=O2 Net=temp<0>
Bit    13744 0x00020000   1328 Block=P10 Latch=O2 Net=Result<3>
Bit    13767 0x00020000   1351 Block=P9 Latch=O2 Net=Result<2>
Bit    18728 0x00040000    104 Block=P3 Latch=I Net=add_IBUF
Bit    18767 0x00040000    143 Block=P2 Latch=I Net=delete_IBUF
Bit    20111 0x00040000   1487 Block=P11 Latch=I Net=load_IBUF
Bit   131034 0x00062200    666 Block=SLICE_X2Y73 Latch=YQ Net=temp<2>
Bit   131069 0x00062200    701 Block=SLICE_X2Y72 Latch=XQ Net=temp<3>
Bit   134170 0x00062400    698 Block=SLICE_X3Y72 Latch=YQ Net=temp<1>
Bit   134173 0x00062400    701 Block=SLICE_X3Y72 Latch=XQ Net=temp<0>
Bit  1083336 0x00280200     40 Block=P89 Latch=I Net=clk_BUFGP/IBUFG
