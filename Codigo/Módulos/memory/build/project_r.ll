Revision 3
; Created by bitgen P.15xf at Thu Jun  6 01:40:48 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit    12656 0x00020000    240 Block=P5 Latch=O2 Net=GLOBAL_LOGIC1
Bit    12679 0x00020000    263 Block=P4 Latch=O2 Net=GLOBAL_LOGIC0
Bit    18728 0x00040000    104 Block=P3 Latch=I Net=cVirgul_IBUF
Bit    18767 0x00040000    143 Block=P2 Latch=I Net=load_IBUF
Bit    19983 0x00040000   1359 Block=P9 Latch=I Net=cEnter_IBUF
Bit  1083336 0x00280200     40 Block=P89 Latch=I Net=clk_BUFGP/IBUFG
