`timescale 1ns / 1ps
module counter_tb;
	wire enter, virgul;
	reg clk, load, cEnter, cVirgul;
	

	memory uut ( .clk(clk), .load(load), .cEnter(cEnter), .cVirgul(cVirgul), .enter(enter), .virgul(virgul));
	
	initial begin  // Initialize Inputs
		clk=0;
		cEnter=0;
		load=1;
		cVirgul=0;
		#10
		load=0;
	end	
     
	initial begin 
		forever begin		
		#100
		cEnter=1;
		#100
		cEnter=0;
		#100
		load=1;
		#100
		cVirgul=1;
		#100		
		load=0;
		#100
		cVirgul=0;
		end
	end 

	initial begin 
		forever begin
		#20;
		clk=1;
		#20
		clk=0;		
		end
	end 
	
	initial begin: TEST_CASE
		$dumpfile("memory_TB.vcd");
		$dumpvars(-1, uut);
		#2000 $finish;
	end

endmodule
