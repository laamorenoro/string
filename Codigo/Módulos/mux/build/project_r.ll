Revision 3
; Created by bitgen P.15xf at Wed Jun  5 17:41:31 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit  2205104 0x04002400   1264 Block=P71 Latch=O2 Net=muxOut_6_OBUF
Bit  2205232 0x04002400   1392 Block=P68 Latch=O2 Net=muxOut_4_OBUF
Bit  2205360 0x04002400   1520 Block=P66 Latch=O2 Net=muxOut_5_OBUF
Bit  2205488 0x04002400   1648 Block=P63 Latch=O2 Net=muxOut_1_OBUF
Bit  2205511 0x04002400   1671 Block=P62 Latch=O2 Net=muxOut_3_OBUF
Bit  2205616 0x04002400   1776 Block=P61 Latch=O2 Net=muxOut_2_OBUF
Bit  2205872 0x04002400   2032 Block=P58 Latch=O2 Net=muxOut_0_OBUF
Bit  2211343 0x04020200   1295 Block=P70 Latch=I Net=addEnhe_IBUF
Bit  2211367 0x04020200   1319 Block=P69 Latch=I Net=char_6_IBUF
Bit  2211471 0x04020200   1423 Block=P67 Latch=I Net=char_5_IBUF
Bit  2211599 0x04020200   1551 Block=P65 Latch=I Net=char_4_IBUF
Bit  2211855 0x04020200   1807 Block=P60 Latch=I Net=char_3_IBUF
Bit  2212111 0x04020200   2063 Block=P57 Latch=I Net=char_2_IBUF
Bit  2213032 0x04020200   2984 Block=P54 Latch=I Net=char_1_IBUF
Bit  2213071 0x04020200   3023 Block=P53 Latch=I Net=char_0_IBUF
