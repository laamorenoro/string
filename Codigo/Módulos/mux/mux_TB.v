`timescale 1ns / 1ps
module mux_tb;
	reg [6:0] char;
	reg addEnhe;
	wire [6:0] muxOut;
	
	mux uut ( .char(char), .addEnhe(addEnhe), .muxOut(muxOut));
	
	initial begin  // Initialize Inputs
		char=7'b0000010;
		addEnhe=0;
	end	
     
	initial begin 
		forever begin		
		#50
		char=7'b0111111;
		#50
		char=7'b0001000;
		addEnhe=1;
		#50
		char=7'b0000010;
		#50
		char=7'b0010000;
		addEnhe=0;
		
		end
	end 

	initial begin: TEST_CASE
		$dumpfile("mux_TB.vcd");
		$dumpvars(-1, uut);
		#2000 $finish;
	end

endmodule
