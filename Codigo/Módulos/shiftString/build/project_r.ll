Revision 3
; Created by bitgen P.15xf at Wed Jun  5 17:48:41 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit    12528 0x00020000    112 Block=C2 Latch=O2 Net=temp_mux0000<31>
Bit    12551 0x00020000    135 Block=C1 Latch=O2 Net=temp_mux0000<32>
Bit    12656 0x00020000    240 Block=D2 Latch=O2 Net=temp_mux0000<38>
Bit    12679 0x00020000    263 Block=D1 Latch=O2 Net=temp_mux0000<47>
Bit    12784 0x00020000    368 Block=E1 Latch=O2 Net=temp_mux0000<46>
Bit    12807 0x00020000    391 Block=E2 Latch=O2 Net=temp_mux0000<41>
Bit    12831 0x00020000    415 Block=F4 Latch=O2 Net=temp_mux0000<36>
Bit    12976 0x00020000    560 Block=F2 Latch=O2 Net=temp_mux0000<34>
Bit    12999 0x00020000    583 Block=F1 Latch=O2 Net=temp_mux0000<35>
Bit    13104 0x00020000    688 Block=G4 Latch=O2 Net=temp_mux0000<45>
Bit    13127 0x00020000    711 Block=G3 Latch=O2 Net=temp_mux0000<49>
Bit    13232 0x00020000    816 Block=G5 Latch=O2 Net=temp_mux0000<42>
Bit    13255 0x00020000    839 Block=G6 Latch=O2 Net=temp_mux0000<43>
Bit    13360 0x00020000    944 Block=H5 Latch=O2 Net=temp_mux0000<44>
Bit    13383 0x00020000    967 Block=H6 Latch=O2 Net=temp_mux0000<50>
Bit    13488 0x00020000   1072 Block=H3 Latch=O2 Net=temp_mux0000<51>
Bit    13511 0x00020000   1095 Block=H4 Latch=O2 Net=temp_mux0000<48>
Bit    13616 0x00020000   1200 Block=H1 Latch=O2 Net=temp_mux0000<54>
Bit    13639 0x00020000   1223 Block=H2 Latch=O2 Net=temp_mux0000<53>
Bit    13744 0x00020000   1328 Block=J4 Latch=O2 Net=temp_mux0000<56>
Bit    13767 0x00020000   1351 Block=J5 Latch=O2 Net=temp_mux0000<57>
Bit    13872 0x00020000   1456 Block=J2 Latch=O2 Net=temp_mux0000<63>
Bit    13895 0x00020000   1479 Block=J1 Latch=O2 Net=temp_mux0000<71>
Bit    14000 0x00020000   1584 Block=K4 Latch=O2 Net=temp_mux0000<62>
Bit    14023 0x00020000   1607 Block=K3 Latch=O2 Net=temp_mux0000<55>
Bit    14128 0x00020000   1712 Block=K5 Latch=O2 Net=temp_mux0000<68>
Bit    14151 0x00020000   1735 Block=K6 Latch=O2 Net=temp_mux0000<69>
Bit    14256 0x00020000   1840 Block=L2 Latch=O2 Net=temp_mux0000<61>
Bit    14279 0x00020000   1863 Block=L1 Latch=O2 Net=temp_mux0000<60>
Bit    14384 0x00020000   1968 Block=L4 Latch=O2 Net=temp_mux0000<52>
Bit    14407 0x00020000   1991 Block=L3 Latch=O2 Net=temp_mux0000<64>
Bit    14512 0x00020000   2096 Block=L5 Latch=O2 Net=temp_mux0000<58>
Bit    14535 0x00020000   2119 Block=L6 Latch=O2 Net=temp_mux0000<65>
Bit    14640 0x00020000   2224 Block=M3 Latch=O2 Net=temp_mux0000<74>
Bit    14663 0x00020000   2247 Block=M4 Latch=O2 Net=temp_mux0000<75>
Bit    14768 0x00020000   2352 Block=M6 Latch=O2 Net=temp_mux0000<76>
Bit    14791 0x00020000   2375 Block=M5 Latch=O2 Net=temp_mux0000<70>
Bit    14896 0x00020000   2480 Block=N5 Latch=O2 Net=temp_mux0000<73>
Bit    14919 0x00020000   2503 Block=N4 Latch=O2 Net=temp_mux0000<67>
Bit    15024 0x00020000   2608 Block=P1 Latch=O2 Net=temp_mux0000<59>
Bit    15047 0x00020000   2631 Block=P2 Latch=O2 Net=temp_mux0000<66>
Bit    15071 0x00020000   2655 Block=R4 Latch=O2 Net=temp_mux0000<72>
Bit    71440 0x00042200     48 Block=B3 Latch=O2 Net=temp_mux0000<37>
Bit    71463 0x00042200     71 Block=C3 Latch=O2 Net=temp_mux0000<30>
Bit    72090 0x00042200    698 Block=SLICE_X0Y72 Latch=YQ Net=temp<48>
Bit    72093 0x00042200    701 Block=SLICE_X0Y72 Latch=XQ Net=temp<49>
Bit    72122 0x00042200    730 Block=SLICE_X0Y71 Latch=YQ Net=temp<46>
Bit    72125 0x00042200    733 Block=SLICE_X0Y71 Latch=XQ Net=temp<47>
Bit    72154 0x00042200    762 Block=SLICE_X0Y70 Latch=YQ Net=temp<44>
Bit    72157 0x00042200    765 Block=SLICE_X0Y70 Latch=XQ Net=temp<45>
Bit    72410 0x00042200   1018 Block=SLICE_X0Y62 Latch=YQ Net=temp<50>
Bit    72413 0x00042200   1021 Block=SLICE_X0Y62 Latch=XQ Net=temp<51>
Bit    72858 0x00042200   1466 Block=SLICE_X0Y48 Latch=YQ Net=temp<52>
Bit    72861 0x00042200   1469 Block=SLICE_X0Y48 Latch=XQ Net=temp<53>
Bit    72922 0x00042200   1530 Block=SLICE_X0Y46 Latch=YQ Net=temp<70>
Bit    72925 0x00042200   1533 Block=SLICE_X0Y46 Latch=XQ Net=temp<71>
Bit    73178 0x00042200   1786 Block=SLICE_X0Y38 Latch=YQ Net=temp<68>
Bit    73181 0x00042200   1789 Block=SLICE_X0Y38 Latch=XQ Net=temp<69>
Bit    73242 0x00042200   1850 Block=SLICE_X0Y36 Latch=YQ Net=temp<60>
Bit    73245 0x00042200   1853 Block=SLICE_X0Y36 Latch=XQ Net=temp<61>
Bit    73274 0x00042200   1882 Block=SLICE_X0Y35 Latch=YQ Net=temp<74>
Bit    73277 0x00042200   1885 Block=SLICE_X0Y35 Latch=XQ Net=temp<75>
Bit    73306 0x00042200   1914 Block=SLICE_X0Y34 Latch=YQ Net=temp<64>
Bit    73309 0x00042200   1917 Block=SLICE_X0Y34 Latch=XQ Net=temp<65>
Bit    73370 0x00042200   1978 Block=SLICE_X0Y32 Latch=YQ Net=temp<58>
Bit    73373 0x00042200   1981 Block=SLICE_X0Y32 Latch=XQ Net=temp<59>
Bit    75002 0x00042400    506 Block=SLICE_X1Y78 Latch=YQ Net=temp<34>
Bit    75005 0x00042400    509 Block=SLICE_X1Y78 Latch=XQ Net=temp<35>
Bit    75034 0x00042400    538 Block=SLICE_X1Y77 Latch=YQ Net=temp<40>
Bit    75037 0x00042400    541 Block=SLICE_X1Y77 Latch=XQ Net=temp<41>
Bit    75226 0x00042400    730 Block=SLICE_X1Y71 Latch=YQ Net=temp<42>
Bit    75229 0x00042400    733 Block=SLICE_X1Y71 Latch=XQ Net=temp<43>
Bit    75674 0x00042400   1178 Block=SLICE_X1Y57 Latch=YQ Net=temp<54>
Bit    75677 0x00042400   1181 Block=SLICE_X1Y57 Latch=XQ Net=temp<55>
Bit    75834 0x00042400   1338 Block=SLICE_X1Y52 Latch=YQ Net=temp<56>
Bit    75837 0x00042400   1341 Block=SLICE_X1Y52 Latch=XQ Net=temp<57>
Bit    76026 0x00042400   1530 Block=SLICE_X1Y46 Latch=YQ Net=temp<62>
Bit    76029 0x00042400   1533 Block=SLICE_X1Y46 Latch=XQ Net=temp<63>
Bit    76282 0x00042400   1786 Block=SLICE_X1Y38 Latch=YQ Net=temp<76>
Bit    76378 0x00042400   1882 Block=SLICE_X1Y35 Latch=YQ Net=temp<66>
Bit    76381 0x00042400   1885 Block=SLICE_X1Y35 Latch=XQ Net=temp<67>
Bit    76474 0x00042400   1978 Block=SLICE_X1Y32 Latch=YQ Net=temp<72>
Bit    76477 0x00042400   1981 Block=SLICE_X1Y32 Latch=XQ Net=temp<73>
Bit   130490 0x00062200    122 Block=SLICE_X2Y90 Latch=YQ Net=temp<32>
Bit   130493 0x00062200    125 Block=SLICE_X2Y90 Latch=XQ Net=temp<33>
Bit   133754 0x00062400    282 Block=SLICE_X3Y85 Latch=YQ Net=temp<38>
Bit   133757 0x00062400    285 Block=SLICE_X3Y85 Latch=XQ Net=temp<39>
Bit   189392 0x00082200     48 Block=B4 Latch=O2 Net=temp_mux0000<39>
Bit   189415 0x00082200     71 Block=A4 Latch=O2 Net=temp_mux0000<40>
Bit   192634 0x00082400    186 Block=SLICE_X5Y88 Latch=YQ Net=temp<36>
Bit   192637 0x00082400    189 Block=SLICE_X5Y88 Latch=XQ Net=temp<37>
Bit   248351 0x000a2200     31 Block=C4 Latch=O2 Net=temp_mux0000<33>
Bit   307344 0x000c2200     48 Block=D5 Latch=O2 Net=temp_mux0000<29>
Bit   307367 0x000c2200     71 Block=C5 Latch=O2 Net=temp_mux0000<28>
Bit   484272 0x00122200     48 Block=A6 Latch=O2 Net=temp_mux0000<27>
Bit   484295 0x00122200     71 Block=B6 Latch=O2 Net=temp_mux0000<26>
Bit   484378 0x00122200    154 Block=SLICE_X12Y89 Latch=YQ Net=temp<30>
Bit   484381 0x00122200    157 Block=SLICE_X12Y89 Latch=XQ Net=temp<31>
Bit   487482 0x00122400    154 Block=SLICE_X13Y89 Latch=YQ Net=temp<26>
Bit   487485 0x00122400    157 Block=SLICE_X13Y89 Latch=XQ Net=temp<27>
Bit   543290 0x00142200     90 Block=SLICE_X14Y91 Latch=YQ Net=temp<28>
Bit   543293 0x00142200     93 Block=SLICE_X14Y91 Latch=XQ Net=temp<29>
Bit   602224 0x00162200     48 Block=E7 Latch=O2 Net=temp_mux0000<25>
Bit   602247 0x00162200     71 Block=F7 Latch=O2 Net=temp_mux0000<5>
Bit   664304 0x00182400     48 Block=D7 Latch=O2 Net=temp_mux0000<12>
Bit   664327 0x00182400     71 Block=C7 Latch=O2 Net=temp_mux0000<24>
Bit   667482 0x001a0000    122 Block=SLICE_X19Y90 Latch=YQ Net=temp<24>
Bit   667485 0x001a0000    125 Block=SLICE_X19Y90 Latch=XQ Net=temp<25>
Bit   723263 0x001a2400     31 Block=A8 Latch=O2 Net=temp_mux0000<19>
Bit   782256 0x001c2400     48 Block=F8 Latch=O2 Net=temp_mux0000<4>
Bit   782279 0x001c2400     71 Block=E8 Latch=O2 Net=temp_mux0000<21>
Bit   782330 0x001c2400    122 Block=SLICE_X22Y90 Latch=YQ Net=temp<4>
Bit   782333 0x001c2400    125 Block=SLICE_X22Y90 Latch=XQ Net=temp<5>
Bit   785466 0x001e0000    154 Block=SLICE_X23Y89 Latch=YQ Net=temp<18>
Bit   785469 0x001e0000    157 Block=SLICE_X23Y89 Latch=XQ Net=temp<19>
Bit   841306 0x001e2400    122 Block=SLICE_X24Y90 Latch=YQ Net=temp<12>
Bit   841309 0x001e2400    125 Block=SLICE_X24Y90 Latch=XQ Net=temp<13>
Bit   844378 0x00200000     90 Block=SLICE_X25Y91 Latch=YQ Net=temp<20>
Bit   844381 0x00200000     93 Block=SLICE_X25Y91 Latch=XQ Net=temp<21>
Bit   844506 0x00200000    218 Block=SLICE_X25Y87 Latch=YQ Net=temp<22>
Bit   844509 0x00200000    221 Block=SLICE_X25Y87 Latch=XQ Net=temp<23>
Bit   900208 0x00202400     48 Block=F9 Latch=O2 Net=temp_mux0000<13>
Bit   900231 0x00202400     71 Block=E9 Latch=O2 Net=temp_mux0000<16>
Bit   965383 0x00240200     39 Block=G9 Latch=I Net=delete_IBUF
Bit  1018160 0x00242400     48 Block=D9 Latch=O2 Net=temp_mux0000<18>
Bit  1018183 0x00242400     71 Block=C9 Latch=O2 Net=temp_mux0000<7>
Bit  1083336 0x00280200     40 Block=B9 Latch=I Net=clk_BUFGP/IBUFG
Bit  1139239 0x002a0000     71 Block=B10 Latch=O2 Net=temp_mux0000<6>
Bit  1142618 0x002a0200    346 Block=SLICE_X35Y83 Latch=YQ Net=temp<16>
Bit  1142621 0x002a0200    349 Block=SLICE_X35Y83 Latch=XQ Net=temp<17>
Bit  1145416 0x002a0400     40 Block=A10 Latch=I Net=muxOut_6_IBUF
Bit  1198175 0x002c0000     31 Block=B11 Latch=O2 Net=temp_mux0000<20>
Bit  1257168 0x002e0000     48 Block=E10 Latch=O2 Net=temp_mux0000<14>
Bit  1257191 0x002e0000     71 Block=D10 Latch=O2 Net=temp_mux0000<0>
Bit  1375120 0x00320000     48 Block=D11 Latch=O2 Net=temp_mux0000<17>
Bit  1375143 0x00320000     71 Block=C11 Latch=O2 Net=temp_mux0000<23>
Bit  1375162 0x00320000     90 Block=SLICE_X42Y91 Latch=YQ Net=temp<6>
Bit  1375165 0x00320000     93 Block=SLICE_X42Y91 Latch=XQ Net=temp<7>
Bit  1375226 0x00320000    154 Block=SLICE_X42Y89 Latch=YQ Net=temp<10>
Bit  1375229 0x00320000    157 Block=SLICE_X42Y89 Latch=XQ Net=temp<11>
Bit  1378266 0x00320200     90 Block=SLICE_X43Y91 Latch=YQ Net=temp<14>
Bit  1378269 0x00320200     93 Block=SLICE_X43Y91 Latch=XQ Net=temp<15>
Bit  1434079 0x00340000     31 Block=A11 Latch=O2 Net=temp_mux0000<22>
Bit  1434138 0x00340000     90 Block=SLICE_X44Y91 Latch=YQ Net=temp<8>
Bit  1434141 0x00340000     93 Block=SLICE_X44Y91 Latch=XQ Net=temp<9>
Bit  1434170 0x00340000    122 Block=SLICE_X44Y90 Latch=YQ Net=temp<2>
Bit  1434173 0x00340000    125 Block=SLICE_X44Y90 Latch=XQ Net=temp<3>
Bit  1437274 0x00340200    122 Block=SLICE_X45Y90 Latch=YQ Net=temp<0>
Bit  1437277 0x00340200    125 Block=SLICE_X45Y90 Latch=XQ Net=temp<1>
Bit  1493072 0x00360000     48 Block=F11 Latch=O2 Net=temp_mux0000<9>
Bit  1493095 0x00360000     71 Block=E11 Latch=O2 Net=temp_mux0000<11>
Bit  1614128 0x003a0200     48 Block=E12 Latch=O2 Net=temp_mux0000<3>
Bit  1614151 0x003a0200     71 Block=F12 Latch=O2 Net=temp_mux0000<2>
Bit  1732080 0x02002400     48 Block=B13 Latch=O2 Net=temp_mux0000<1>
Bit  1732103 0x02002400     71 Block=A13 Latch=O2 Net=temp_mux0000<10>
Bit  1791056 0x02004a00     48 Block=A14 Latch=O2 Net=temp_mux0000<15>
Bit  1791079 0x02004a00     71 Block=B14 Latch=O2 Net=temp_mux0000<8>
Bit  1856231 0x02007400     39 Block=E13 Latch=I Net=muxOut_5_IBUF
Bit  1915208 0x02020200     40 Block=C14 Latch=I Net=load_IBUF
Bit  1915247 0x02020200     79 Block=D14 Latch=I Net=muxOut_3_IBUF
Bit  1974184 0x02022800     40 Block=A15 Latch=I Net=muxOut_2_IBUF
Bit  1974223 0x02022800     79 Block=B15 Latch=I Net=muxOut_4_IBUF
Bit  2092136 0x02027400     40 Block=A16 Latch=I Net=muxOut_0_IBUF
Bit  2092175 0x02027400     79 Block=B16 Latch=I Net=muxOut_1_IBUF
Bit  2151111 0x04000200     39 Block=C15 Latch=I Net=add_IBUF
