`timescale 1ns / 1ps
module shiftString_tb;
	reg load, add, clk, delete;
	reg [6:0] muxOut;
	wire [69:0] string;

	

	shiftString uut ( .load(load), .clk(clk), .add(add), .delete(delete), .muxOut(muxOut), .string(string));
	
	initial begin  // Initialize Inputs
		add=0;
		delete=0;
		load=0;
		muxOut=7'b1111111;
	end	
     
	initial begin 
		forever begin		
		
		#100
		load=1;
		#40;
		load=0;
		#40;
		add=1;
		#40
		add=0;
		muxOut=7'b0111111;
		#40
		add=1;
		#40
		add=0;
		#40
		delete=1;
		
		end
	end 

	initial begin 
		forever begin
		#20
		clk=1;
		#20
		clk=0;		
		end
	end 
	
	initial begin: TEST_CASE
		$dumpfile("shiftString_TB.vcd");
		$dumpvars(-1, uut);
		#2000 $finish;
	end

endmodule
