Revision 3
; Created by bitgen P.15xf at Sun Jun  2 00:05:04 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit    14640 0x00020000   2224 Block=M3 Latch=O2 Net=change_OBUF
Bit   257576 0x000c0000   3048 Block=T4 Latch=I Net=new_IBUF
Bit   257615 0x000c0000   3087 Block=U4 Latch=I Net=char_0_IBUF
Bit   375567 0x00100000   3087 Block=R5 Latch=I Net=char_1_IBUF
Bit   493480 0x00140000   3048 Block=P6 Latch=I Net=char_4_IBUF
Bit   493519 0x00140000   3087 Block=R6 Latch=I Net=char_3_IBUF
Bit   552455 0x00160000   3047 Block=U6 Latch=I Net=char_2_IBUF
Bit   605232 0x00162200   3056 Block=P7 Latch=O2 Net=shiftString1/temp_mux0000<0>
Bit   605255 0x00162200   3079 Block=N7 Latch=O2 Net=shiftString1/temp_mux0000<3>
Bit   673512 0x001a0200   3048 Block=R7 Latch=I Net=char_5_IBUF
Bit   673551 0x001a0200   3087 Block=T7 Latch=I Net=char_6_IBUF
Bit   726288 0x001a2400   3056 Block=N8 Latch=O2 Net=shiftString1/temp_mux0000<2>
Bit   726311 0x001a2400   3079 Block=P8 Latch=O2 Net=shiftString1/temp_mux0000<1>
Bit   785247 0x001c2400   3039 Block=P9 Latch=O2 Net=control1/next_state_mux0000<0>11
Bit   844240 0x001e2400   3056 Block=T8 Latch=O2 Net=shiftString1/temp_mux0000<5>
Bit   844263 0x001e2400   3079 Block=R8 Latch=O2 Net=shiftString1/temp_mux0000<4>
Bit   902461 0x00202400   2301 Block=SLICE_X26Y22 Latch=XQ Net=control1/next_state<1>
Bit   961562 0x00222400   2426 Block=SLICE_X28Y18 Latch=YQ Net=control1/next_state<6>
Bit   962192 0x00222400   3056 Block=M9 Latch=O2 Net=shiftString1/temp_mux0000<7>
Bit   962215 0x00222400   3079 Block=N9 Latch=O2 Net=shiftString1/temp_mux0000<6>
Bit  1021151 0x00242400   3039 Block=R9 Latch=O2 Net=shiftString1/temp_mux0000<8>
Bit  1080144 0x00262400   3056 Block=V9 Latch=O2 Net=shiftString1/temp_mux0000<10>
Bit  1080167 0x00262400   3079 Block=U9 Latch=O2 Net=shiftString1/temp_mux0000<9>
Bit  1082458 0x00280000   2266 Block=SLICE_X33Y23 Latch=YQ Net=control1/enter
Bit  1083336 0x00280200     40 Block=B9 Latch=I Net=clk_BUFGP/IBUFG
Bit  1144573 0x002a0200   2301 Block=SLICE_X35Y22 Latch=XQ Net=counter1/temp<3>
Bit  1144602 0x002a0200   2330 Block=SLICE_X35Y21 Latch=YQ Net=counter1/temp<2>
Bit  1200442 0x002c0000   2298 Block=SLICE_X36Y22 Latch=YQ Net=counter1/temp<1>
Bit  1200445 0x002c0000   2301 Block=SLICE_X36Y22 Latch=XQ Net=counter1/temp<0>
Bit  1201200 0x002c0000   3056 Block=P10 Latch=O2 Net=shiftString1/temp_mux0000<12>
Bit  1201223 0x002c0000   3079 Block=R10 Latch=O2 Net=shiftString1/temp_mux0000<11>
Bit  1259450 0x002e0000   2330 Block=SLICE_X38Y21 Latch=YQ Net=control1/virgul
Bit  1260159 0x002e0000   3039 Block=V11 Latch=O2 Net=shiftString1/temp_mux0000<13>
Bit  1318461 0x00300000   2365 Block=SLICE_X40Y20 Latch=XQ Net=control1/next_state<2>
Bit  1319152 0x00300000   3056 Block=N10 Latch=O2 Net=shiftString1/temp_mux0000<15>
Bit  1319175 0x00300000   3079 Block=M10 Latch=O2 Net=shiftString1/temp_mux0000<14>
Bit  1321562 0x00300200   2362 Block=SLICE_X41Y20 Latch=YQ Net=control1/next_state<7>
Bit  1321754 0x00300200   2554 Block=SLICE_X41Y14 Latch=YQ Net=shiftString1/temp<6>
Bit  1321757 0x00300200   2557 Block=SLICE_X41Y14 Latch=XQ Net=shiftString1/temp<7>
Bit  1377498 0x00320000   2426 Block=SLICE_X42Y18 Latch=YQ Net=shiftString1/temp<0>
Bit  1377501 0x00320000   2429 Block=SLICE_X42Y18 Latch=XQ Net=shiftString1/temp<1>
Bit  1380570 0x00320200   2394 Block=SLICE_X43Y19 Latch=YQ Net=shiftString1/temp<2>
Bit  1380573 0x00320200   2397 Block=SLICE_X43Y19 Latch=XQ Net=shiftString1/temp<3>
Bit  1436442 0x00340000   2394 Block=SLICE_X44Y19 Latch=YQ Net=shiftString1/temp<4>
Bit  1436445 0x00340000   2397 Block=SLICE_X44Y19 Latch=XQ Net=shiftString1/temp<5>
Bit  1436602 0x00340000   2554 Block=SLICE_X44Y14 Latch=YQ Net=shiftString1/temp<10>
Bit  1436605 0x00340000   2557 Block=SLICE_X44Y14 Latch=XQ Net=shiftString1/temp<11>
Bit  1437104 0x00340000   3056 Block=N11 Latch=O2 Net=shiftString1/temp_mux0000<17>
Bit  1437127 0x00340000   3079 Block=P11 Latch=O2 Net=shiftString1/temp_mux0000<16>
Bit  1439578 0x00340200   2426 Block=SLICE_X45Y18 Latch=YQ Net=shiftString1/temp<8>
Bit  1439581 0x00340200   2429 Block=SLICE_X45Y18 Latch=XQ Net=shiftString1/temp<9>
Bit  1496063 0x00360000   3039 Block=R11 Latch=O2 Net=shiftString1/temp_mux0000<18>
Bit  1498522 0x00360200   2394 Block=SLICE_X47Y19 Latch=YQ Net=shiftString1/temp<12>
Bit  1498525 0x00360200   2397 Block=SLICE_X47Y19 Latch=XQ Net=shiftString1/temp<13>
Bit  1555056 0x00380000   3056 Block=V13 Latch=O2 Net=shiftString1/temp_mux0000<20>
Bit  1555079 0x00380000   3079 Block=V12 Latch=O2 Net=shiftString1/temp_mux0000<19>
Bit  1557498 0x00380200   2394 Block=SLICE_X49Y19 Latch=YQ Net=shiftString1/temp<14>
Bit  1557501 0x00380200   2397 Block=SLICE_X49Y19 Latch=XQ Net=shiftString1/temp<15>
Bit  1616218 0x003a0200   2138 Block=SLICE_X50Y27 Latch=YQ Net=control1/next_state<5>
Bit  1616346 0x003a0200   2266 Block=SLICE_X50Y23 Latch=YQ Net=control1/next_state<4>
Bit  1616445 0x003a0200   2365 Block=SLICE_X50Y20 Latch=XQ Net=control1/next_state<3>
Bit  1616474 0x003a0200   2394 Block=SLICE_X50Y19 Latch=YQ Net=shiftString1/temp<16>
Bit  1616477 0x003a0200   2397 Block=SLICE_X50Y19 Latch=XQ Net=shiftString1/temp<17>
Bit  1617136 0x003a0200   3056 Block=R12 Latch=O2 Net=shiftString1/temp_mux0000<22>
Bit  1617159 0x003a0200   3079 Block=T12 Latch=O2 Net=shiftString1/temp_mux0000<21>
Bit  1619325 0x003a0400   2141 Block=SLICE_X51Y27 Latch=XQ Net=shiftString1/temp<76>
Bit  1619578 0x003a0400   2394 Block=SLICE_X51Y19 Latch=YQ Net=shiftString1/temp<18>
Bit  1619581 0x003a0400   2397 Block=SLICE_X51Y19 Latch=XQ Net=shiftString1/temp<19>
Bit  1734394 0x02002400   2362 Block=SLICE_X54Y20 Latch=YQ Net=shiftString1/temp<28>
Bit  1734397 0x02002400   2365 Block=SLICE_X54Y20 Latch=XQ Net=shiftString1/temp<29>
Bit  1734426 0x02002400   2394 Block=SLICE_X54Y19 Latch=YQ Net=shiftString1/temp<26>
Bit  1734429 0x02002400   2397 Block=SLICE_X54Y19 Latch=XQ Net=shiftString1/temp<27>
Bit  1734458 0x02002400   2426 Block=SLICE_X54Y18 Latch=YQ Net=shiftString1/temp<22>
Bit  1734461 0x02002400   2429 Block=SLICE_X54Y18 Latch=XQ Net=shiftString1/temp<23>
Bit  1734554 0x02002400   2522 Block=SLICE_X54Y15 Latch=YQ Net=shiftString1/temp<30>
Bit  1734557 0x02002400   2525 Block=SLICE_X54Y15 Latch=XQ Net=shiftString1/temp<31>
Bit  1735088 0x02002400   3056 Block=R13 Latch=O2 Net=shiftString1/temp_mux0000<24>
Bit  1735111 0x02002400   3079 Block=P13 Latch=O2 Net=shiftString1/temp_mux0000<23>
Bit  1737530 0x02002600   2394 Block=SLICE_X55Y19 Latch=YQ Net=shiftString1/temp<20>
Bit  1737533 0x02002600   2397 Block=SLICE_X55Y19 Latch=XQ Net=shiftString1/temp<21>
Bit  1737562 0x02002600   2426 Block=SLICE_X55Y18 Latch=YQ Net=shiftString1/temp<24>
Bit  1737565 0x02002600   2429 Block=SLICE_X55Y18 Latch=XQ Net=shiftString1/temp<25>
Bit  1853040 0x02007000   3056 Block=R14 Latch=O2 Net=shiftString1/temp_mux0000<26>
Bit  1853063 0x02007000   3079 Block=T14 Latch=O2 Net=shiftString1/temp_mux0000<25>
Bit  1911999 0x02009600   3039 Block=T15 Latch=O2 Net=shiftString1/temp_mux0000<27>
Bit  1970992 0x02022400   3056 Block=U15 Latch=O2 Net=shiftString1/temp_mux0000<29>
Bit  1971015 0x02022400   3079 Block=V15 Latch=O2 Net=shiftString1/temp_mux0000<28>
Bit  2087066 0x02027000   1178 Block=SLICE_X64Y57 Latch=YQ Net=shiftString1/temp<74>
Bit  2087069 0x02027000   1181 Block=SLICE_X64Y57 Latch=XQ Net=shiftString1/temp<75>
Bit  2087098 0x02027000   1210 Block=SLICE_X64Y56 Latch=YQ Net=shiftString1/temp<72>
Bit  2087101 0x02027000   1213 Block=SLICE_X64Y56 Latch=XQ Net=shiftString1/temp<73>
Bit  2087226 0x02027000   1338 Block=SLICE_X64Y52 Latch=YQ Net=shiftString1/temp<70>
Bit  2087229 0x02027000   1341 Block=SLICE_X64Y52 Latch=XQ Net=shiftString1/temp<71>
Bit  2087258 0x02027000   1370 Block=SLICE_X64Y51 Latch=YQ Net=shiftString1/temp<66>
Bit  2087261 0x02027000   1373 Block=SLICE_X64Y51 Latch=XQ Net=shiftString1/temp<67>
Bit  2087290 0x02027000   1402 Block=SLICE_X64Y50 Latch=YQ Net=shiftString1/temp<58>
Bit  2087293 0x02027000   1405 Block=SLICE_X64Y50 Latch=XQ Net=shiftString1/temp<59>
Bit  2087322 0x02027000   1434 Block=SLICE_X64Y49 Latch=YQ Net=shiftString1/temp<60>
Bit  2087325 0x02027000   1437 Block=SLICE_X64Y49 Latch=XQ Net=shiftString1/temp<61>
Bit  2087354 0x02027000   1466 Block=SLICE_X64Y48 Latch=YQ Net=shiftString1/temp<68>
Bit  2087357 0x02027000   1469 Block=SLICE_X64Y48 Latch=XQ Net=shiftString1/temp<69>
Bit  2087386 0x02027000   1498 Block=SLICE_X64Y47 Latch=YQ Net=shiftString1/temp<56>
Bit  2087389 0x02027000   1501 Block=SLICE_X64Y47 Latch=XQ Net=shiftString1/temp<57>
Bit  2087642 0x02027000   1754 Block=SLICE_X64Y39 Latch=YQ Net=shiftString1/temp<50>
Bit  2087645 0x02027000   1757 Block=SLICE_X64Y39 Latch=XQ Net=shiftString1/temp<51>
Bit  2087674 0x02027000   1786 Block=SLICE_X64Y38 Latch=YQ Net=shiftString1/temp<52>
Bit  2087677 0x02027000   1789 Block=SLICE_X64Y38 Latch=XQ Net=shiftString1/temp<53>
Bit  2087770 0x02027000   1882 Block=SLICE_X64Y35 Latch=YQ Net=shiftString1/temp<48>
Bit  2087773 0x02027000   1885 Block=SLICE_X64Y35 Latch=XQ Net=shiftString1/temp<49>
Bit  2087898 0x02027000   2010 Block=SLICE_X64Y31 Latch=YQ Net=shiftString1/temp<46>
Bit  2087901 0x02027000   2013 Block=SLICE_X64Y31 Latch=XQ Net=shiftString1/temp<47>
Bit  2088186 0x02027000   2298 Block=SLICE_X64Y22 Latch=YQ Net=shiftString1/temp<42>
Bit  2088189 0x02027000   2301 Block=SLICE_X64Y22 Latch=XQ Net=shiftString1/temp<43>
Bit  2088218 0x02027000   2330 Block=SLICE_X64Y21 Latch=YQ Net=shiftString1/temp<38>
Bit  2088221 0x02027000   2333 Block=SLICE_X64Y21 Latch=XQ Net=shiftString1/temp<39>
Bit  2088250 0x02027000   2362 Block=SLICE_X64Y20 Latch=YQ Net=shiftString1/temp<36>
Bit  2088253 0x02027000   2365 Block=SLICE_X64Y20 Latch=XQ Net=shiftString1/temp<37>
Bit  2088314 0x02027000   2426 Block=SLICE_X64Y18 Latch=YQ Net=shiftString1/temp<32>
Bit  2088317 0x02027000   2429 Block=SLICE_X64Y18 Latch=XQ Net=shiftString1/temp<33>
Bit  2090330 0x02027200   1338 Block=SLICE_X65Y52 Latch=YQ Net=shiftString1/temp<64>
Bit  2090333 0x02027200   1341 Block=SLICE_X65Y52 Latch=XQ Net=shiftString1/temp<65>
Bit  2090458 0x02027200   1466 Block=SLICE_X65Y48 Latch=YQ Net=shiftString1/temp<62>
Bit  2090461 0x02027200   1469 Block=SLICE_X65Y48 Latch=XQ Net=shiftString1/temp<63>
Bit  2090522 0x02027200   1530 Block=SLICE_X65Y46 Latch=YQ Net=shiftString1/temp<54>
Bit  2090525 0x02027200   1533 Block=SLICE_X65Y46 Latch=XQ Net=shiftString1/temp<55>
Bit  2091130 0x02027200   2138 Block=SLICE_X65Y27 Latch=YQ Net=shiftString1/temp<44>
Bit  2091133 0x02027200   2141 Block=SLICE_X65Y27 Latch=XQ Net=shiftString1/temp<45>
Bit  2091322 0x02027200   2330 Block=SLICE_X65Y21 Latch=YQ Net=shiftString1/temp<40>
Bit  2091325 0x02027200   2333 Block=SLICE_X65Y21 Latch=XQ Net=shiftString1/temp<41>
Bit  2091354 0x02027200   2362 Block=SLICE_X65Y20 Latch=YQ Net=shiftString1/temp<34>
Bit  2091357 0x02027200   2365 Block=SLICE_X65Y20 Latch=XQ Net=shiftString1/temp<35>
Bit  2147920 0x02029600   3056 Block=U16 Latch=O2 Net=shiftString1/temp_mux0000<31>
Bit  2147943 0x02029600   3079 Block=T16 Latch=O2 Net=shiftString1/temp_mux0000<30>
Bit  2204039 0x04002400    199 Block=C18 Latch=O2 Net=shiftString1/temp_mux0000<57>
Bit  2204144 0x04002400    304 Block=D16 Latch=O2 Net=shiftString1/temp_mux0000<75>
Bit  2204167 0x04002400    327 Block=D17 Latch=O2 Net=shiftString1/temp_mux0000<74>
Bit  2204255 0x04002400    415 Block=E17 Latch=O2 Net=shiftString1/temp_mux0000<73>
Bit  2204336 0x04002400    496 Block=F14 Latch=O2 Net=shiftString1/temp_mux0000<72>
Bit  2204359 0x04002400    519 Block=F15 Latch=O2 Net=shiftString1/temp_mux0000<71>
Bit  2204464 0x04002400    624 Block=G13 Latch=O2 Net=shiftString1/temp_mux0000<70>
Bit  2204487 0x04002400    647 Block=G14 Latch=O2 Net=shiftString1/temp_mux0000<69>
Bit  2204592 0x04002400    752 Block=F17 Latch=O2 Net=shiftString1/temp_mux0000<68>
Bit  2204615 0x04002400    775 Block=F18 Latch=O2 Net=shiftString1/temp_mux0000<67>
Bit  2204720 0x04002400    880 Block=G16 Latch=O2 Net=shiftString1/temp_mux0000<66>
Bit  2204743 0x04002400    903 Block=G15 Latch=O2 Net=shiftString1/temp_mux0000<65>
Bit  2204848 0x04002400   1008 Block=H15 Latch=O2 Net=shiftString1/temp_mux0000<64>
Bit  2204871 0x04002400   1031 Block=H14 Latch=O2 Net=shiftString1/temp_mux0000<63>
Bit  2204976 0x04002400   1136 Block=H17 Latch=O2 Net=shiftString1/temp_mux0000<62>
Bit  2204999 0x04002400   1159 Block=H16 Latch=O2 Net=shiftString1/temp_mux0000<61>
Bit  2205104 0x04002400   1264 Block=J13 Latch=O2 Net=shiftString1/temp_mux0000<60>
Bit  2205127 0x04002400   1287 Block=J12 Latch=O2 Net=shiftString1/temp_mux0000<59>
Bit  2205232 0x04002400   1392 Block=J14 Latch=O2 Net=shiftString1/temp_mux0000<58>
Bit  2205255 0x04002400   1415 Block=J15 Latch=O2 Net=shiftString1/temp_mux0000<54>
Bit  2205360 0x04002400   1520 Block=J16 Latch=O2 Net=shiftString1/temp_mux0000<56>
Bit  2205383 0x04002400   1543 Block=J17 Latch=O2 Net=shiftString1/temp_mux0000<55>
Bit  2205488 0x04002400   1648 Block=K14 Latch=O2 Net=shiftString1/temp_mux0000<50>
Bit  2205511 0x04002400   1671 Block=K15 Latch=O2 Net=shiftString1/temp_mux0000<53>
Bit  2205616 0x04002400   1776 Block=K12 Latch=O2 Net=shiftString1/temp_mux0000<52>
Bit  2205639 0x04002400   1799 Block=K13 Latch=O2 Net=shiftString1/temp_mux0000<51>
Bit  2205744 0x04002400   1904 Block=L17 Latch=O2 Net=shiftString1/temp_mux0000<48>
Bit  2205767 0x04002400   1927 Block=L18 Latch=O2 Net=shiftString1/temp_mux0000<49>
Bit  2205872 0x04002400   2032 Block=L15 Latch=O2 Net=shiftString1/temp_mux0000<76>
Bit  2205895 0x04002400   2055 Block=L16 Latch=O2 Net=shiftString1/temp_mux0000<47>
Bit  2206000 0x04002400   2160 Block=M18 Latch=O2 Net=shiftString1/temp_mux0000<46>
Bit  2206023 0x04002400   2183 Block=N18 Latch=O2 Net=shiftString1/temp_mux0000<45>
Bit  2206128 0x04002400   2288 Block=M16 Latch=O2 Net=shiftString1/temp_mux0000<44>
Bit  2206151 0x04002400   2311 Block=M15 Latch=O2 Net=shiftString1/temp_mux0000<43>
Bit  2206256 0x04002400   2416 Block=P18 Latch=O2 Net=shiftString1/temp_mux0000<42>
Bit  2206279 0x04002400   2439 Block=P17 Latch=O2 Net=shiftString1/temp_mux0000<41>
Bit  2206384 0x04002400   2544 Block=M13 Latch=O2 Net=shiftString1/temp_mux0000<40>
Bit  2206407 0x04002400   2567 Block=M14 Latch=O2 Net=shiftString1/temp_mux0000<39>
Bit  2206495 0x04002400   2655 Block=P15 Latch=O2 Net=shiftString1/temp_mux0000<38>
Bit  2206576 0x04002400   2736 Block=R16 Latch=O2 Net=shiftString1/temp_mux0000<37>
Bit  2206599 0x04002400   2759 Block=R15 Latch=O2 Net=shiftString1/temp_mux0000<36>
Bit  2206704 0x04002400   2864 Block=T18 Latch=O2 Net=shiftString1/temp_mux0000<35>
Bit  2206727 0x04002400   2887 Block=R18 Latch=O2 Net=shiftString1/temp_mux0000<34>
Bit  2206832 0x04002400   2992 Block=T17 Latch=O2 Net=shiftString1/temp_mux0000<33>
Bit  2206855 0x04002400   3015 Block=U18 Latch=O2 Net=shiftString1/temp_mux0000<32>
Bit  2208768 0x04020000   1824 Block=K17 Latch=IQ1 Net=control1/next_state<0>
Bit  2211879 0x04020200   1831 Block=K17 Latch=I Net=btn1_IBUF
