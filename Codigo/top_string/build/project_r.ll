Revision 3
; Created by bitgen P.15xf at Wed Jun  5 16:56:22 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit    14663 0x00020000   2247 Block=M4 Latch=O2 Net=change_OBUF
Bit    20584 0x00040000   1960 Block=L4 Latch=I Net=btn1_IBUF
Bit   198639 0x000a0000   3087 Block=V4 Latch=I Net=char_1_IBUF
Bit   257576 0x000c0000   3048 Block=T4 Latch=I Net=new_IBUF
Bit   257615 0x000c0000   3087 Block=U4 Latch=I Net=char_2_IBUF
Bit   310335 0x000c2200   3039 Block=U5 Latch=O2 Net=complete_OBUF
Bit   375567 0x00100000   3087 Block=R5 Latch=I Net=char_0_IBUF
Bit   493519 0x00140000   3087 Block=R6 Latch=I Net=char_3_IBUF
Bit   605232 0x00162200   3056 Block=P7 Latch=O2 Net=shiftString1/temp_mux0000<5>
Bit   611471 0x00180000   3087 Block=N7 Latch=I Net=char_4_IBUF
Bit   673512 0x001a0200   3048 Block=R7 Latch=I Net=char_6_IBUF
Bit   673551 0x001a0200   3087 Block=T7 Latch=I Net=char_5_IBUF
Bit   726288 0x001a2400   3056 Block=N8 Latch=O2 Net=shiftString1/temp_mux0000<0>
Bit   726311 0x001a2400   3079 Block=P8 Latch=O2 Net=shiftString1/temp_mux0000<2>
Bit   785247 0x001c2400   3039 Block=P9 Latch=O2 Net=shiftString1/temp_mux0000<9>
Bit   844240 0x001e2400   3056 Block=T8 Latch=O2 Net=shiftString1/temp_mux0000<15>
Bit   844263 0x001e2400   3079 Block=R8 Latch=O2 Net=shiftString1/temp_mux0000<6>
Bit   962192 0x00222400   3056 Block=M9 Latch=O2 Net=shiftString1/temp_mux0000<12>
Bit   962215 0x00222400   3079 Block=N9 Latch=O2 Net=shiftString1/temp_mux0000<14>
Bit  1021151 0x00242400   3039 Block=R9 Latch=O2 Net=shiftString1/temp_mux0000<22>
Bit  1080144 0x00262400   3056 Block=V9 Latch=O2 Net=shiftString1/temp_mux0000<13>
Bit  1080167 0x00262400   3079 Block=U9 Latch=O2 Net=shiftString1/temp_mux0000<1>
Bit  1083336 0x00280200     40 Block=B9 Latch=I Net=clk_BUFGP/IBUFG
Bit  1201200 0x002c0000   3056 Block=P10 Latch=O2 Net=shiftString1/temp_mux0000<20>
Bit  1201223 0x002c0000   3079 Block=R10 Latch=O2 Net=shiftString1/temp_mux0000<11>
Bit  1203962 0x002c0200   2714 Block=SLICE_X37Y9 Latch=YQ Net=shiftString1/temp<14>
Bit  1203965 0x002c0200   2717 Block=SLICE_X37Y9 Latch=XQ Net=shiftString1/temp<15>
Bit  1260159 0x002e0000   3039 Block=V11 Latch=O2 Net=shiftString1/temp_mux0000<3>
Bit  1318746 0x00300000   2650 Block=SLICE_X40Y11 Latch=YQ Net=shiftString1/temp<12>
Bit  1318749 0x00300000   2653 Block=SLICE_X40Y11 Latch=XQ Net=shiftString1/temp<13>
Bit  1319152 0x00300000   3056 Block=N10 Latch=O2 Net=shiftString1/temp_mux0000<4>
Bit  1319175 0x00300000   3079 Block=M10 Latch=O2 Net=shiftString1/temp_mux0000<10>
Bit  1321533 0x00300200   2333 Block=SLICE_X41Y21 Latch=XQ Net=memory1/tempEnter
Bit  1380506 0x00320200   2330 Block=SLICE_X43Y21 Latch=YQ Net=memory1/tempVirgul
Bit  1435965 0x00340000   1917 Block=SLICE_X44Y34 Latch=XQ Net=counter1/temp<3>
Bit  1436509 0x00340000   2461 Block=SLICE_X44Y17 Latch=XQ Net=shiftString1/temp<0>
Bit  1436666 0x00340000   2618 Block=SLICE_X44Y12 Latch=YQ Net=shiftString1/temp<22>
Bit  1436669 0x00340000   2621 Block=SLICE_X44Y12 Latch=XQ Net=shiftString1/temp<23>
Bit  1437104 0x00340000   3056 Block=N11 Latch=O2 Net=shiftString1/temp_mux0000<8>
Bit  1437127 0x00340000   3079 Block=P11 Latch=O2 Net=shiftString1/temp_mux0000<18>
Bit  1439066 0x00340200   1914 Block=SLICE_X45Y34 Latch=YQ Net=counter1/temp<2>
Bit  1439709 0x00340200   2557 Block=SLICE_X45Y14 Latch=XQ Net=shiftString1/temp<6>
Bit  1439741 0x00340200   2589 Block=SLICE_X45Y13 Latch=XQ Net=shiftString1/temp<5>
Bit  1495645 0x00360000   2621 Block=SLICE_X46Y12 Latch=XQ Net=shiftString1/temp<4>
Bit  1495677 0x00360000   2653 Block=SLICE_X46Y11 Latch=XQ Net=shiftString1/temp<1>
Bit  1495706 0x00360000   2682 Block=SLICE_X46Y10 Latch=YQ Net=shiftString1/temp<8>
Bit  1496063 0x00360000   3039 Block=R11 Latch=O2 Net=shiftString1/temp_mux0000<27>
Bit  1498010 0x00360200   1882 Block=SLICE_X47Y35 Latch=YQ Net=counter1/temp<1>
Bit  1498013 0x00360200   1885 Block=SLICE_X47Y35 Latch=XQ Net=counter1/temp<0>
Bit  1554589 0x00380000   2589 Block=SLICE_X48Y13 Latch=XQ Net=shiftString1/temp<3>
Bit  1554618 0x00380000   2618 Block=SLICE_X48Y12 Latch=YQ Net=shiftString1/temp<10>
Bit  1554621 0x00380000   2621 Block=SLICE_X48Y12 Latch=XQ Net=shiftString1/temp<11>
Bit  1555056 0x00380000   3056 Block=V13 Latch=O2 Net=shiftString1/temp_mux0000<7>
Bit  1555079 0x00380000   3079 Block=V12 Latch=O2 Net=shiftString1/temp_mux0000<26>
Bit  1616058 0x003a0200   1978 Block=SLICE_X50Y32 Latch=YQ Net=shiftString1/temp<76>
Bit  1616445 0x003a0200   2365 Block=SLICE_X50Y20 Latch=XQ Net=shiftString1/temp<2>
Bit  1617136 0x003a0200   3056 Block=R12 Latch=O2 Net=shiftString1/temp_mux0000<17>
Bit  1617159 0x003a0200   3079 Block=T12 Latch=O2 Net=shiftString1/temp_mux0000<23>
Bit  1619197 0x003a0400   2013 Block=SLICE_X51Y31 Latch=XQ Net=control1/next_state_FSM_FFd3
Bit  1619293 0x003a0400   2109 Block=SLICE_X51Y28 Latch=XQ Net=control1/next_state_FSM_FFd1
Bit  1619674 0x003a0400   2490 Block=SLICE_X51Y16 Latch=YQ Net=shiftString1/temp<7>
Bit  1619770 0x003a0400   2586 Block=SLICE_X51Y13 Latch=YQ Net=shiftString1/temp<16>
Bit  1619773 0x003a0400   2589 Block=SLICE_X51Y13 Latch=XQ Net=shiftString1/temp<17>
Bit  1675069 0x003c0200   2013 Block=SLICE_X52Y31 Latch=XQ Net=shiftString1/temp<9>
Bit  1678173 0x02000000   2013 Block=SLICE_X53Y31 Latch=XQ Net=control1/next_state_FSM_FFd2
Bit  1732080 0x02002400     48 Block=B13 Latch=O2 Net=shiftString1/temp_mux0000<74>
Bit  1732103 0x02002400     71 Block=A13 Latch=O2 Net=shiftString1/temp_mux0000<73>
Bit  1732826 0x02002400    794 Block=SLICE_X54Y69 Latch=YQ Net=shiftString1/temp<72>
Bit  1732829 0x02002400    797 Block=SLICE_X54Y69 Latch=XQ Net=shiftString1/temp<73>
Bit  1734650 0x02002400   2618 Block=SLICE_X54Y12 Latch=YQ Net=shiftString1/temp<24>
Bit  1734653 0x02002400   2621 Block=SLICE_X54Y12 Latch=XQ Net=shiftString1/temp<25>
Bit  1735088 0x02002400   3056 Block=R13 Latch=O2 Net=shiftString1/temp_mux0000<24>
Bit  1735111 0x02002400   3079 Block=P13 Latch=O2 Net=shiftString1/temp_mux0000<25>
Bit  1736346 0x02002600   1210 Block=SLICE_X55Y56 Latch=YQ Net=shiftString1/temp<74>
Bit  1736349 0x02002600   1213 Block=SLICE_X55Y56 Latch=XQ Net=shiftString1/temp<75>
Bit  1736730 0x02002600   1594 Block=SLICE_X55Y44 Latch=YQ Net=shiftString1/temp<70>
Bit  1736733 0x02002600   1597 Block=SLICE_X55Y44 Latch=XQ Net=shiftString1/temp<71>
Bit  1737594 0x02002600   2458 Block=SLICE_X55Y17 Latch=YQ Net=shiftString1/temp<26>
Bit  1737597 0x02002600   2461 Block=SLICE_X55Y17 Latch=XQ Net=shiftString1/temp<27>
Bit  1737626 0x02002600   2490 Block=SLICE_X55Y16 Latch=YQ Net=shiftString1/temp<20>
Bit  1737629 0x02002600   2493 Block=SLICE_X55Y16 Latch=XQ Net=shiftString1/temp<21>
Bit  1737754 0x02002600   2618 Block=SLICE_X55Y12 Latch=YQ Net=shiftString1/temp<18>
Bit  1737757 0x02002600   2621 Block=SLICE_X55Y12 Latch=XQ Net=shiftString1/temp<19>
Bit  1737818 0x02002600   2682 Block=SLICE_X55Y10 Latch=YQ Net=shiftString1/temp<28>
Bit  1737821 0x02002600   2685 Block=SLICE_X55Y10 Latch=XQ Net=shiftString1/temp<29>
Bit  1853040 0x02007000   3056 Block=R14 Latch=O2 Net=shiftString1/temp_mux0000<40>
Bit  1853063 0x02007000   3079 Block=T14 Latch=O2 Net=shiftString1/temp_mux0000<29>
Bit  1911999 0x02009600   3039 Block=T15 Latch=O2 Net=shiftString1/temp_mux0000<28>
Bit  1970992 0x02022400   3056 Block=U15 Latch=O2 Net=shiftString1/temp_mux0000<35>
Bit  1971015 0x02022400   3079 Block=V15 Latch=O2 Net=shiftString1/temp_mux0000<16>
Bit  2087162 0x02027000   1274 Block=SLICE_X64Y54 Latch=YQ Net=shiftString1/temp<50>
Bit  2087165 0x02027000   1277 Block=SLICE_X64Y54 Latch=XQ Net=shiftString1/temp<51>
Bit  2087322 0x02027000   1434 Block=SLICE_X64Y49 Latch=YQ Net=shiftString1/temp<56>
Bit  2087325 0x02027000   1437 Block=SLICE_X64Y49 Latch=XQ Net=shiftString1/temp<57>
Bit  2087418 0x02027000   1530 Block=SLICE_X64Y46 Latch=YQ Net=shiftString1/temp<58>
Bit  2087421 0x02027000   1533 Block=SLICE_X64Y46 Latch=XQ Net=shiftString1/temp<59>
Bit  2087450 0x02027000   1562 Block=SLICE_X64Y45 Latch=YQ Net=shiftString1/temp<54>
Bit  2087453 0x02027000   1565 Block=SLICE_X64Y45 Latch=XQ Net=shiftString1/temp<55>
Bit  2088378 0x02027000   2490 Block=SLICE_X64Y16 Latch=YQ Net=shiftString1/temp<34>
Bit  2088381 0x02027000   2493 Block=SLICE_X64Y16 Latch=XQ Net=shiftString1/temp<35>
Bit  2088570 0x02027000   2682 Block=SLICE_X64Y10 Latch=YQ Net=shiftString1/temp<40>
Bit  2088573 0x02027000   2685 Block=SLICE_X64Y10 Latch=XQ Net=shiftString1/temp<41>
Bit  2089882 0x02027200    890 Block=SLICE_X65Y66 Latch=YQ Net=shiftString1/temp<64>
Bit  2089885 0x02027200    893 Block=SLICE_X65Y66 Latch=XQ Net=shiftString1/temp<65>
Bit  2090138 0x02027200   1146 Block=SLICE_X65Y58 Latch=YQ Net=shiftString1/temp<66>
Bit  2090141 0x02027200   1149 Block=SLICE_X65Y58 Latch=XQ Net=shiftString1/temp<67>
Bit  2146458 0x02029600   1594 Block=SLICE_X66Y44 Latch=YQ Net=shiftString1/temp<62>
Bit  2146461 0x02029600   1597 Block=SLICE_X66Y44 Latch=XQ Net=shiftString1/temp<63>
Bit  2146778 0x02029600   1914 Block=SLICE_X66Y34 Latch=YQ Net=shiftString1/temp<48>
Bit  2146781 0x02029600   1917 Block=SLICE_X66Y34 Latch=XQ Net=shiftString1/temp<49>
Bit  2147354 0x02029600   2490 Block=SLICE_X66Y16 Latch=YQ Net=shiftString1/temp<38>
Bit  2147357 0x02029600   2493 Block=SLICE_X66Y16 Latch=XQ Net=shiftString1/temp<39>
Bit  2147482 0x02029600   2618 Block=SLICE_X66Y12 Latch=YQ Net=shiftString1/temp<30>
Bit  2147485 0x02029600   2621 Block=SLICE_X66Y12 Latch=XQ Net=shiftString1/temp<31>
Bit  2147920 0x02029600   3056 Block=U16 Latch=O2 Net=shiftString1/temp_mux0000<31>
Bit  2147943 0x02029600   3079 Block=T16 Latch=O2 Net=shiftString1/temp_mux0000<30>
Bit  2149210 0x04000000   1242 Block=SLICE_X67Y55 Latch=YQ Net=shiftString1/temp<60>
Bit  2149213 0x04000000   1245 Block=SLICE_X67Y55 Latch=XQ Net=shiftString1/temp<61>
Bit  2149242 0x04000000   1274 Block=SLICE_X67Y54 Latch=YQ Net=shiftString1/temp<46>
Bit  2149245 0x04000000   1277 Block=SLICE_X67Y54 Latch=XQ Net=shiftString1/temp<47>
Bit  2149370 0x04000000   1402 Block=SLICE_X67Y50 Latch=YQ Net=shiftString1/temp<68>
Bit  2149373 0x04000000   1405 Block=SLICE_X67Y50 Latch=XQ Net=shiftString1/temp<69>
Bit  2149626 0x04000000   1658 Block=SLICE_X67Y42 Latch=YQ Net=shiftString1/temp<52>
Bit  2149629 0x04000000   1661 Block=SLICE_X67Y42 Latch=XQ Net=shiftString1/temp<53>
Bit  2150074 0x04000000   2106 Block=SLICE_X67Y28 Latch=YQ Net=shiftString1/temp<44>
Bit  2150077 0x04000000   2109 Block=SLICE_X67Y28 Latch=XQ Net=shiftString1/temp<45>
Bit  2150266 0x04000000   2298 Block=SLICE_X67Y22 Latch=YQ Net=shiftString1/temp<42>
Bit  2150269 0x04000000   2301 Block=SLICE_X67Y22 Latch=XQ Net=shiftString1/temp<43>
Bit  2150458 0x04000000   2490 Block=SLICE_X67Y16 Latch=YQ Net=shiftString1/temp<32>
Bit  2150461 0x04000000   2493 Block=SLICE_X67Y16 Latch=XQ Net=shiftString1/temp<33>
Bit  2150650 0x04000000   2682 Block=SLICE_X67Y10 Latch=YQ Net=shiftString1/temp<36>
Bit  2150653 0x04000000   2685 Block=SLICE_X67Y10 Latch=XQ Net=shiftString1/temp<37>
Bit  2204144 0x04002400    304 Block=D16 Latch=O2 Net=shiftString1/temp_mux0000<71>
Bit  2204255 0x04002400    415 Block=E17 Latch=O2 Net=shiftString1/temp_mux0000<47>
Bit  2204336 0x04002400    496 Block=F14 Latch=O2 Net=shiftString1/temp_mux0000<75>
Bit  2204359 0x04002400    519 Block=F15 Latch=O2 Net=shiftString1/temp_mux0000<66>
Bit  2204464 0x04002400    624 Block=G13 Latch=O2 Net=shiftString1/temp_mux0000<54>
Bit  2204487 0x04002400    647 Block=G14 Latch=O2 Net=shiftString1/temp_mux0000<72>
Bit  2204592 0x04002400    752 Block=F17 Latch=O2 Net=shiftString1/temp_mux0000<56>
Bit  2204615 0x04002400    775 Block=F18 Latch=O2 Net=shiftString1/temp_mux0000<70>
Bit  2204720 0x04002400    880 Block=G16 Latch=O2 Net=shiftString1/temp_mux0000<59>
Bit  2204743 0x04002400    903 Block=G15 Latch=O2 Net=shiftString1/temp_mux0000<64>
Bit  2204848 0x04002400   1008 Block=H15 Latch=O2 Net=shiftString1/temp_mux0000<65>
Bit  2204871 0x04002400   1031 Block=H14 Latch=O2 Net=shiftString1/temp_mux0000<51>
Bit  2204976 0x04002400   1136 Block=H17 Latch=O2 Net=shiftString1/temp_mux0000<63>
Bit  2204999 0x04002400   1159 Block=H16 Latch=O2 Net=shiftString1/temp_mux0000<67>
Bit  2205104 0x04002400   1264 Block=J13 Latch=O2 Net=shiftString1/temp_mux0000<61>
Bit  2205127 0x04002400   1287 Block=J12 Latch=O2 Net=shiftString1/temp_mux0000<60>
Bit  2205232 0x04002400   1392 Block=J14 Latch=O2 Net=shiftString1/temp_mux0000<68>
Bit  2205255 0x04002400   1415 Block=J15 Latch=O2 Net=shiftString1/temp_mux0000<69>
Bit  2205360 0x04002400   1520 Block=J16 Latch=O2 Net=shiftString1/temp_mux0000<50>
Bit  2205383 0x04002400   1543 Block=J17 Latch=O2 Net=shiftString1/temp_mux0000<57>
Bit  2205488 0x04002400   1648 Block=K14 Latch=O2 Net=shiftString1/temp_mux0000<58>
Bit  2205511 0x04002400   1671 Block=K15 Latch=O2 Net=shiftString1/temp_mux0000<52>
Bit  2205616 0x04002400   1776 Block=K12 Latch=O2 Net=shiftString1/temp_mux0000<46>
Bit  2205639 0x04002400   1799 Block=K13 Latch=O2 Net=shiftString1/temp_mux0000<53>
Bit  2205744 0x04002400   1904 Block=L17 Latch=O2 Net=shiftString1/temp_mux0000<48>
Bit  2205767 0x04002400   1927 Block=L18 Latch=O2 Net=shiftString1/temp_mux0000<49>
Bit  2205872 0x04002400   2032 Block=L15 Latch=O2 Net=shiftString1/temp_mux0000<44>
Bit  2205895 0x04002400   2055 Block=L16 Latch=O2 Net=shiftString1/temp_mux0000<76>
Bit  2206000 0x04002400   2160 Block=M18 Latch=O2 Net=shiftString1/temp_mux0000<55>
Bit  2206023 0x04002400   2183 Block=N18 Latch=O2 Net=shiftString1/temp_mux0000<62>
Bit  2206128 0x04002400   2288 Block=M16 Latch=O2 Net=shiftString1/temp_mux0000<42>
Bit  2206151 0x04002400   2311 Block=M15 Latch=O2 Net=shiftString1/temp_mux0000<43>
Bit  2206256 0x04002400   2416 Block=P18 Latch=O2 Net=shiftString1/temp_mux0000<21>
Bit  2206279 0x04002400   2439 Block=P17 Latch=O2 Net=shiftString1/temp_mux0000<45>
Bit  2206384 0x04002400   2544 Block=M13 Latch=O2 Net=shiftString1/temp_mux0000<39>
Bit  2206407 0x04002400   2567 Block=M14 Latch=O2 Net=shiftString1/temp_mux0000<32>
Bit  2206495 0x04002400   2655 Block=P15 Latch=O2 Net=shiftString1/temp_mux0000<38>
Bit  2206576 0x04002400   2736 Block=R16 Latch=O2 Net=shiftString1/temp_mux0000<34>
Bit  2206599 0x04002400   2759 Block=R15 Latch=O2 Net=shiftString1/temp_mux0000<36>
Bit  2206704 0x04002400   2864 Block=T18 Latch=O2 Net=shiftString1/temp_mux0000<33>
Bit  2206727 0x04002400   2887 Block=R18 Latch=O2 Net=shiftString1/temp_mux0000<37>
Bit  2206832 0x04002400   2992 Block=T17 Latch=O2 Net=shiftString1/temp_mux0000<41>
Bit  2206855 0x04002400   3015 Block=U18 Latch=O2 Net=shiftString1/temp_mux0000<19>
