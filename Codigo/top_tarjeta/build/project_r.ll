Revision 3
; Created by bitgen P.15xf at Wed Jun  5 16:15:26 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit    12831 0x00020000    415 Block=F4 Latch=O2 Net=top1/shiftString1/temp<6>
Bit    15071 0x00020000   2655 Block=R4 Latch=O2 Net=GLOBAL_LOGIC1
Bit   666234 0x00182400   1978 Block=SLICE_X18Y32 Latch=YQ Net=clkcount1/temp
Bit   669050 0x001a0000   1690 Block=SLICE_X19Y41 Latch=YQ Net=clkcount1/counter<27>
Bit   669053 0x001a0000   1693 Block=SLICE_X19Y41 Latch=XQ Net=clkcount1/counter<26>
Bit   669082 0x001a0000   1722 Block=SLICE_X19Y40 Latch=YQ Net=clkcount1/counter<25>
Bit   669085 0x001a0000   1725 Block=SLICE_X19Y40 Latch=XQ Net=clkcount1/counter<24>
Bit   669114 0x001a0000   1754 Block=SLICE_X19Y39 Latch=YQ Net=clkcount1/counter<23>
Bit   669117 0x001a0000   1757 Block=SLICE_X19Y39 Latch=XQ Net=clkcount1/counter<22>
Bit   669146 0x001a0000   1786 Block=SLICE_X19Y38 Latch=YQ Net=clkcount1/counter<21>
Bit   669149 0x001a0000   1789 Block=SLICE_X19Y38 Latch=XQ Net=clkcount1/counter<20>
Bit   669178 0x001a0000   1818 Block=SLICE_X19Y37 Latch=YQ Net=clkcount1/counter<19>
Bit   669181 0x001a0000   1821 Block=SLICE_X19Y37 Latch=XQ Net=clkcount1/counter<18>
Bit   669210 0x001a0000   1850 Block=SLICE_X19Y36 Latch=YQ Net=clkcount1/counter<17>
Bit   669213 0x001a0000   1853 Block=SLICE_X19Y36 Latch=XQ Net=clkcount1/counter<16>
Bit   669242 0x001a0000   1882 Block=SLICE_X19Y35 Latch=YQ Net=clkcount1/counter<15>
Bit   669245 0x001a0000   1885 Block=SLICE_X19Y35 Latch=XQ Net=clkcount1/counter<14>
Bit   669274 0x001a0000   1914 Block=SLICE_X19Y34 Latch=YQ Net=clkcount1/counter<13>
Bit   669277 0x001a0000   1917 Block=SLICE_X19Y34 Latch=XQ Net=clkcount1/counter<12>
Bit   669306 0x001a0000   1946 Block=SLICE_X19Y33 Latch=YQ Net=clkcount1/counter<11>
Bit   669309 0x001a0000   1949 Block=SLICE_X19Y33 Latch=XQ Net=clkcount1/counter<10>
Bit   669338 0x001a0000   1978 Block=SLICE_X19Y32 Latch=YQ Net=clkcount1/counter<9>
Bit   669341 0x001a0000   1981 Block=SLICE_X19Y32 Latch=XQ Net=clkcount1/counter<8>
Bit   669370 0x001a0000   2010 Block=SLICE_X19Y31 Latch=YQ Net=clkcount1/counter<7>
Bit   669373 0x001a0000   2013 Block=SLICE_X19Y31 Latch=XQ Net=clkcount1/counter<6>
Bit   669402 0x001a0000   2042 Block=SLICE_X19Y30 Latch=YQ Net=clkcount1/counter<5>
Bit   669405 0x001a0000   2045 Block=SLICE_X19Y30 Latch=XQ Net=clkcount1/counter<4>
Bit   669434 0x001a0000   2074 Block=SLICE_X19Y29 Latch=YQ Net=clkcount1/counter<3>
Bit   669437 0x001a0000   2077 Block=SLICE_X19Y29 Latch=XQ Net=clkcount1/counter<2>
Bit   669466 0x001a0000   2106 Block=SLICE_X19Y28 Latch=YQ Net=clkcount1/counter<1>
Bit   669469 0x001a0000   2109 Block=SLICE_X19Y28 Latch=XQ Net=clkcount1/counter<0>
Bit   901914 0x00202400   1754 Block=SLICE_X26Y39 Latch=YQ Net=clkcount1/tempSnew
Bit  1083375 0x00280200     79 Block=B8 Latch=I Net=clk_BUFGP/IBUFG
Bit  1435322 0x00340000   1274 Block=SLICE_X44Y54 Latch=YQ Net=top1/counter1/temp<1>
Bit  1435325 0x00340000   1277 Block=SLICE_X44Y54 Latch=XQ Net=top1/counter1/temp<0>
Bit  1435389 0x00340000   1341 Block=SLICE_X44Y52 Latch=XQ Net=top1/counter1/temp<3>
Bit  1438426 0x00340200   1274 Block=SLICE_X45Y54 Latch=YQ Net=top1/counter1/temp<2>
Bit  1494714 0x00360000   1690 Block=SLICE_X46Y41 Latch=YQ Net=ctrlstate1/state_FSM_FFd5
Bit  1615386 0x003a0200   1306 Block=SLICE_X50Y53 Latch=YQ Net=top1/control1/next_state_FSM_FFd3
Bit  1615453 0x003a0200   1373 Block=SLICE_X50Y51 Latch=XQ Net=top1/control1/next_state_FSM_FFd2
Bit  1615546 0x003a0200   1466 Block=SLICE_X50Y48 Latch=YQ Net=top1/memory1/tempVirgul
Bit  1615741 0x003a0200   1661 Block=SLICE_X50Y42 Latch=XQ Net=top1/memory1/tempEnter
Bit  1615805 0x003a0200   1725 Block=SLICE_X50Y40 Latch=XQ Net=ctrlstate1/state_FSM_FFd4
Bit  1618589 0x003a0400   1405 Block=SLICE_X51Y50 Latch=XQ Net=top1/control1/next_state_FSM_FFd1
Bit  1618845 0x003a0400   1661 Block=SLICE_X51Y42 Latch=XQ Net=ctrlstate1/state_FSM_FFd1
Bit  1618906 0x003a0400   1722 Block=SLICE_X51Y40 Latch=YQ Net=ctrlstate1/state_FSM_FFd2
Bit  1618909 0x003a0400   1725 Block=SLICE_X51Y40 Latch=XQ Net=ctrlstate1/state_FSM_FFd3
Bit  1673754 0x003c0200    698 Block=SLICE_X52Y72 Latch=YQ Net=top1/shiftString1/temp<58>
Bit  1673757 0x003c0200    701 Block=SLICE_X52Y72 Latch=XQ Net=top1/shiftString1/temp<59>
Bit  1673786 0x003c0200    730 Block=SLICE_X52Y71 Latch=YQ Net=top1/shiftString1/temp<60>
Bit  1673789 0x003c0200    733 Block=SLICE_X52Y71 Latch=XQ Net=top1/shiftString1/temp<61>
Bit  1673818 0x003c0200    762 Block=SLICE_X52Y70 Latch=YQ Net=top1/shiftString1/temp<44>
Bit  1673821 0x003c0200    765 Block=SLICE_X52Y70 Latch=XQ Net=top1/shiftString1/temp<45>
Bit  1673850 0x003c0200    794 Block=SLICE_X52Y69 Latch=YQ Net=top1/shiftString1/temp<50>
Bit  1673853 0x003c0200    797 Block=SLICE_X52Y69 Latch=XQ Net=top1/shiftString1/temp<51>
Bit  1673882 0x003c0200    826 Block=SLICE_X52Y68 Latch=YQ Net=top1/shiftString1/temp<22>
Bit  1673885 0x003c0200    829 Block=SLICE_X52Y68 Latch=XQ Net=top1/shiftString1/temp<23>
Bit  1673914 0x003c0200    858 Block=SLICE_X52Y67 Latch=YQ Net=top1/shiftString1/temp<14>
Bit  1673917 0x003c0200    861 Block=SLICE_X52Y67 Latch=XQ Net=top1/shiftString1/temp<15>
Bit  1673946 0x003c0200    890 Block=SLICE_X52Y66 Latch=YQ Net=top1/shiftString1/temp<26>
Bit  1673949 0x003c0200    893 Block=SLICE_X52Y66 Latch=XQ Net=top1/shiftString1/temp<27>
Bit  1673978 0x003c0200    922 Block=SLICE_X52Y65 Latch=YQ Net=top1/shiftString1/temp<34>
Bit  1673981 0x003c0200    925 Block=SLICE_X52Y65 Latch=XQ Net=top1/shiftString1/temp<35>
Bit  1674010 0x003c0200    954 Block=SLICE_X52Y64 Latch=YQ Net=top1/shiftString1/temp<54>
Bit  1674013 0x003c0200    957 Block=SLICE_X52Y64 Latch=XQ Net=top1/shiftString1/temp<55>
Bit  1674042 0x003c0200    986 Block=SLICE_X52Y63 Latch=YQ Net=top1/shiftString1/temp<62>
Bit  1674045 0x003c0200    989 Block=SLICE_X52Y63 Latch=XQ Net=top1/shiftString1/temp<63>
Bit  1674074 0x003c0200   1018 Block=SLICE_X52Y62 Latch=YQ Net=top1/shiftString1/temp<56>
Bit  1674077 0x003c0200   1021 Block=SLICE_X52Y62 Latch=XQ Net=top1/shiftString1/temp<57>
Bit  1674106 0x003c0200   1050 Block=SLICE_X52Y61 Latch=YQ Net=top1/shiftString1/temp<6>
Bit  1674109 0x003c0200   1053 Block=SLICE_X52Y61 Latch=XQ Net=top1/shiftString1/temp<7>
Bit  1674237 0x003c0200   1181 Block=SLICE_X52Y57 Latch=XQ Net=top1/shiftString1/temp<9>
Bit  1676826 0x02000000    666 Block=SLICE_X53Y73 Latch=YQ Net=top1/shiftString1/temp<66>
Bit  1676829 0x02000000    669 Block=SLICE_X53Y73 Latch=XQ Net=top1/shiftString1/temp<67>
Bit  1676890 0x02000000    730 Block=SLICE_X53Y71 Latch=YQ Net=top1/shiftString1/temp<74>
Bit  1676893 0x02000000    733 Block=SLICE_X53Y71 Latch=XQ Net=top1/shiftString1/temp<75>
Bit  1676922 0x02000000    762 Block=SLICE_X53Y70 Latch=YQ Net=top1/shiftString1/temp<68>
Bit  1676925 0x02000000    765 Block=SLICE_X53Y70 Latch=XQ Net=top1/shiftString1/temp<69>
Bit  1676954 0x02000000    794 Block=SLICE_X53Y69 Latch=YQ Net=top1/shiftString1/temp<36>
Bit  1676957 0x02000000    797 Block=SLICE_X53Y69 Latch=XQ Net=top1/shiftString1/temp<37>
Bit  1676986 0x02000000    826 Block=SLICE_X53Y68 Latch=YQ Net=top1/shiftString1/temp<76>
Bit  1677018 0x02000000    858 Block=SLICE_X53Y67 Latch=YQ Net=top1/shiftString1/temp<20>
Bit  1677021 0x02000000    861 Block=SLICE_X53Y67 Latch=XQ Net=top1/shiftString1/temp<21>
Bit  1677050 0x02000000    890 Block=SLICE_X53Y66 Latch=YQ Net=top1/shiftString1/temp<12>
Bit  1677053 0x02000000    893 Block=SLICE_X53Y66 Latch=XQ Net=top1/shiftString1/temp<13>
Bit  1677082 0x02000000    922 Block=SLICE_X53Y65 Latch=YQ Net=top1/shiftString1/temp<42>
Bit  1677085 0x02000000    925 Block=SLICE_X53Y65 Latch=XQ Net=top1/shiftString1/temp<43>
Bit  1677114 0x02000000    954 Block=SLICE_X53Y64 Latch=YQ Net=top1/shiftString1/temp<28>
Bit  1677117 0x02000000    957 Block=SLICE_X53Y64 Latch=XQ Net=top1/shiftString1/temp<29>
Bit  1677146 0x02000000    986 Block=SLICE_X53Y63 Latch=YQ Net=top1/shiftString1/temp<64>
Bit  1677149 0x02000000    989 Block=SLICE_X53Y63 Latch=XQ Net=top1/shiftString1/temp<65>
Bit  1677178 0x02000000   1018 Block=SLICE_X53Y62 Latch=YQ Net=top1/shiftString1/temp<48>
Bit  1677181 0x02000000   1021 Block=SLICE_X53Y62 Latch=XQ Net=top1/shiftString1/temp<49>
Bit  1677242 0x02000000   1082 Block=SLICE_X53Y60 Latch=YQ Net=top1/shiftString1/temp<70>
Bit  1677245 0x02000000   1085 Block=SLICE_X53Y60 Latch=XQ Net=top1/shiftString1/temp<71>
Bit  1732794 0x02002400    762 Block=SLICE_X54Y70 Latch=YQ Net=top1/shiftString1/temp<46>
Bit  1732797 0x02002400    765 Block=SLICE_X54Y70 Latch=XQ Net=top1/shiftString1/temp<47>
Bit  1732826 0x02002400    794 Block=SLICE_X54Y69 Latch=YQ Net=top1/shiftString1/temp<24>
Bit  1732829 0x02002400    797 Block=SLICE_X54Y69 Latch=XQ Net=top1/shiftString1/temp<25>
Bit  1732858 0x02002400    826 Block=SLICE_X54Y68 Latch=YQ Net=top1/shiftString1/temp<16>
Bit  1732861 0x02002400    829 Block=SLICE_X54Y68 Latch=XQ Net=top1/shiftString1/temp<17>
Bit  1732890 0x02002400    858 Block=SLICE_X54Y67 Latch=YQ Net=top1/shiftString1/temp<10>
Bit  1732893 0x02002400    861 Block=SLICE_X54Y67 Latch=XQ Net=top1/shiftString1/temp<11>
Bit  1732922 0x02002400    890 Block=SLICE_X54Y66 Latch=YQ Net=top1/shiftString1/temp<18>
Bit  1732925 0x02002400    893 Block=SLICE_X54Y66 Latch=XQ Net=top1/shiftString1/temp<19>
Bit  1735802 0x02002600    666 Block=SLICE_X55Y73 Latch=YQ Net=top1/shiftString1/temp<52>
Bit  1735805 0x02002600    669 Block=SLICE_X55Y73 Latch=XQ Net=top1/shiftString1/temp<53>
Bit  1735834 0x02002600    698 Block=SLICE_X55Y72 Latch=YQ Net=top1/shiftString1/temp<72>
Bit  1735837 0x02002600    701 Block=SLICE_X55Y72 Latch=XQ Net=top1/shiftString1/temp<73>
Bit  1735898 0x02002600    762 Block=SLICE_X55Y70 Latch=YQ Net=top1/shiftString1/temp<38>
Bit  1735901 0x02002600    765 Block=SLICE_X55Y70 Latch=XQ Net=top1/shiftString1/temp<39>
Bit  1735962 0x02002600    826 Block=SLICE_X55Y68 Latch=YQ Net=top1/shiftString1/temp<30>
Bit  1735965 0x02002600    829 Block=SLICE_X55Y68 Latch=XQ Net=top1/shiftString1/temp<31>
Bit  1735994 0x02002600    858 Block=SLICE_X55Y67 Latch=YQ Net=top1/shiftString1/temp<32>
Bit  1735997 0x02002600    861 Block=SLICE_X55Y67 Latch=XQ Net=top1/shiftString1/temp<33>
Bit  1736026 0x02002600    890 Block=SLICE_X55Y66 Latch=YQ Net=top1/shiftString1/temp<4>
Bit  1736029 0x02002600    893 Block=SLICE_X55Y66 Latch=XQ Net=top1/shiftString1/temp<8>
Bit  1736061 0x02002600    925 Block=SLICE_X55Y65 Latch=XQ Net=top1/shiftString1/temp<5>
Bit  1736090 0x02002600    954 Block=SLICE_X55Y64 Latch=YQ Net=top1/shiftString1/temp<40>
Bit  1736093 0x02002600    957 Block=SLICE_X55Y64 Latch=XQ Net=top1/shiftString1/temp<41>
Bit  1736186 0x02002600   1050 Block=SLICE_X55Y61 Latch=YQ Net=top1/shiftString1/temp<0>
Bit  1736189 0x02002600   1053 Block=SLICE_X55Y61 Latch=XQ Net=top1/shiftString1/temp<1>
Bit  1736314 0x02002600   1178 Block=SLICE_X55Y57 Latch=YQ Net=top1/shiftString1/temp<2>
Bit  1736317 0x02002600   1181 Block=SLICE_X55Y57 Latch=XQ Net=top1/shiftString1/temp<3>
Bit  2204255 0x04002400    415 Block=E17 Latch=O2 Net=top1/shiftString1/temp<4>
Bit  2205232 0x04002400   1392 Block=J14 Latch=O2 Net=top1/shiftString1/temp<0>
Bit  2205255 0x04002400   1415 Block=J15 Latch=O2 Net=top1/shiftString1/temp<1>
Bit  2205488 0x04002400   1648 Block=K14 Latch=O2 Net=top1/shiftString1/temp<3>
Bit  2205511 0x04002400   1671 Block=K15 Latch=O2 Net=top1/shiftString1/temp<2>
Bit  2205616 0x04002400   1776 Block=K12 Latch=O2 Net=JA_1_OBUF
Bit  2205639 0x04002400   1799 Block=K13 Latch=O2 Net=JA_4_OBUF
Bit  2205744 0x04002400   1904 Block=L17 Latch=O2 Net=JA_2_OBUF
Bit  2205872 0x04002400   2032 Block=L15 Latch=O2 Net=JA_0_OBUF
Bit  2205895 0x04002400   2055 Block=L16 Latch=O2 Net=JA_5_OBUF
Bit  2206151 0x04002400   2311 Block=M15 Latch=O2 Net=JA_3_OBUF
Bit  2206407 0x04002400   2567 Block=M14 Latch=O2 Net=JA_6_OBUF
Bit  2206495 0x04002400   2655 Block=P15 Latch=O2 Net=top1/shiftString1/temp<5>
Bit  2210407 0x04020200    359 Block=D18 Latch=I Net=btn1_IBUF
