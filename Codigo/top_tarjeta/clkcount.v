module clkcount(clk, load, reset, ready, sendNew);

	input clk, load, reset;
	output reg ready, sendNew;
	
	reg [27:0] counter;
	reg temp, tempSnew;

	always @(posedge clk) begin
		if (load | reset) begin 
				counter=28'h0000000;
				//temp=0;
			end 
		else begin 
				//temp=0;
				counter=counter+1'b1;
			end
		if (counter==28'h5F5E100) temp=1;
		else temp=0;
		if (counter<=28'h0000001) tempSnew=1;
		else tempSnew=0;
	end 
	
	always @(temp or tempSnew) begin 
			ready<=temp;
			sendNew<=tempSnew;
			end 
	
endmodule
