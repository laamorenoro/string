module clkcount(clk, load, ready);

	input clk, load;
	output reg ready;
	
	reg [27:0] counter;
	reg temp;

	always @(posedge clk) begin
		if (load) begin 
				counter=28'h0000000;
				temp=0;
			 end else if (counter==28'h8000000) begin
					temp=1;
					counter=28'h0000000;
				  end else begin 
						temp=0;
						counter=counter+1'b1;
					   end
	end 
	
	always @(temp) ready<=temp;
	
endmodule
