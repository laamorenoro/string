`timescale 1ns / 1ps
module top_string_tb;
	reg clk, load;
	wire ready;
	

	clkcount uut ( .clk(clk), .load(load), .ready(ready));
	
	initial begin  // Initialize Inputs
		clk=0;
		load=1;
	end	
     
	initial begin 
		#2
		load=0;	
	end 
	
	initial begin 
		forever begin
		#1
		clk=1;
		#1
		clk=0;		
		end
	end 
	
	initial begin: TEST_CASE
		$dumpfile("clkcount_TB.vcd");
		$dumpvars(-1, uut);
		#3000000000 $finish;
	end

endmodule
