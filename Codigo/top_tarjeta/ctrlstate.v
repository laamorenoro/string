module ctrlstate(reset, clk, ready, addO, addDelete, addVirgul, addP, addEne, addEnter,addOne, load, new, sendNew);

	input reset, clk, ready, sendNew;
	output reg load, new;
	output reg addO, addDelete, addVirgul, addP, addEne, addEnter,addOne;	

	reg [4:0] state;
	//reg [4:0] past_state;
	
	parameter s0=5'b00000, s1=5'b00001, s2=5'b00010, s3=5'b00011, s4=5'b00100, s5=5'b00101, s6=5'b00110, s7=5'b00111, s8=5'b01000, s9=5'b01001, s10=5'b01010, s11=5'b01011, s12=5'b01100, s13=5'b01101, s14=5'b01110, s15=5'b01111, s16=5'b10000, s17=5'b10001, s18=5'b10010;
	
/*
	initial begin 
		state=s0;
		past_state=s9;
	end	
*/
	always @(posedge clk) begin
	 	if (reset) state=s0;
		else begin case (state) 
			s0: begin
				if(sendNew==1) state=s10;
				else if(ready==1) state=s1;
					else state=state;
			end			
			s1: begin
				if(sendNew==1) state=s11;
				else if(ready==1) state=s2;
					else state=state;
			end
			s2: begin
				if(sendNew==1) state=s12;
				else if(ready==1) state=s3;
					else state=state;
			end
			s3: begin
				if(sendNew==1) state=s13;
				else if(ready==1) state=s4;
					else state=state;
			end
			s4: begin
				if(sendNew==1) state=s14;
				else if(ready==1) state=s5;
					else state=state;
			end
			s5: begin
				if(sendNew==1) state=s15;
				else if(ready==1) state=s6;
					else state=state;
			end
			s6: begin
				if(sendNew==1) state=s16;
				else if(ready==1) state=s7;
					else state=state;
			end
			s7: begin
				if(sendNew==1) state=s17;
				else if(ready==1) state=s8;
					else state=state;
			end
			s8: begin
				if(sendNew==1) state=s18;
				else if(ready==1) state=s0;
					else state=state;
			end
			s10: begin 
				if(sendNew==0) state=s0;
				else state=state;
			end 
			s11: begin 
				if(sendNew==0) state=s1;
				else state=state;
			end 
			s12: begin 
				if(sendNew==0) state=s2;
				else state=state;
			end 
			s13: begin 
				if(sendNew==0) state=s3;
				else state=state;
			end 
			s14: begin 
				if(sendNew==0) state=s4;
				else state=state;
			end 
			s15: begin 
				if(sendNew==0) state=s5;
				else state=state;
			end 
			s16: begin 
				if(sendNew==0) state=s6;
				else state=state;
			end 
			s17: begin 
				if(sendNew==0) state=s7;
				else state=state;
			end 
			s18: begin 
				if(sendNew==0) state=s8;
				else state=state;
			end 
			default: state=state;
		endcase
		end
	end
	
	always @(state or ready) begin
		case (state)
			s0: begin
				addO=1;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=0;
			end
			s1: begin
				addO=0;
				addDelete=1;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=0;
			end
			s2: begin
				addO=0;
				addDelete=0;	
				addVirgul=1;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=0;
			end
			s3: begin
				addO=0;
				addDelete=0;	
				addVirgul=0;
				addP=1;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=0;
			end
			s4: begin
				addO=0;
				addDelete=0;	
				addVirgul=1;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=0;
			end
			s5: begin
				addO=0;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=1;
				addEnter=0;
				addOne=0;
				new=0;
			end
			s6: begin
				addO=0;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=1;
				addOne=0;
				new=0;
			end
			s7: begin
				addO=1;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=0;
			end
			s8: begin
				addO=0;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=1;
				new=0;
			end
			s10: begin
				addO=1;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=1;
			end
			s11: begin
				addO=0;
				addDelete=1;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=1;
			end
			s12: begin
				addO=0;
				addDelete=0;	
				addVirgul=1;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=1;
			end
			s13: begin
				addO=0;
				addDelete=0;	
				addVirgul=0;
				addP=1;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=1;
			end
			s14: begin
				addO=0;
				addDelete=0;	
				addVirgul=1;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=1;
			end
			s15: begin
				addO=0;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=1;
				addEnter=0;
				addOne=0;
				new=1;
			end
			s16: begin
				addO=0;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=1;
				addOne=0;
				new=1;
			end
			s17: begin
				addO=1;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=1;
			end
			s18: begin
				addO=0;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=1;
				new=1;
			end
			default: begin
				addO=0;
				addDelete=0;	
				addVirgul=0;
				addP=0;
				addEne=0;
				addEnter=0;
				addOne=0;
				new=0;
			end
		endcase	
		if (ready) load=1;
		else load=0;
	end
endmodule
