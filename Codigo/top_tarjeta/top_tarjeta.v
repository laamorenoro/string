module top_tarjeta(clk, btn1, Led, Led1, JA);

	input clk, btn1;
	output [6:0] Led;
	output [6:0] JA;
	output Led1;	
	
	wire [6:0] wchar;
	wire wnew, wsendnew;
	wire [76:0] wstring;
	//wire wreset;
	wire wcomplete;
	wire wchange;
	wire wAddO, wAddDelete, wAddVirgul, wAddP, wAddEne, wAddEnter, wAddOne;
	//wire wReady
	wire wload;

	top_string top1 (.char(wchar), .clk(clk), .new(wnew), .change(wchange), .complete(wcomplete), .btn1(btn1), .string(wstring));
	final final1 (.string(wstring), .leds(Led), .clk(clk)); //, .Led1(Led1));.complete(wcomplete), 
	init init1 (.char(wchar), .addO(wAddO), .addDelete(wAddDelete), .addVirgul(wAddVirgul), .addP(wAddP), .addEne(wAddEne), .addEnter(wAddEnter), .addOne(wAddOne), .JA(JA));// .new(wnew));
	ctrlstate ctrlstate1 (.ready(Led1), .reset(btn1), .clk(clk), .addO(wAddO), .addDelete(wAddDelete), .addVirgul(wAddVirgul), .addP(wAddP), .addEne(wAddEne), .addEnter(wAddEnter), .addOne(wAddOne), .load(wload), .sendNew(wsendnew), .new(wnew));
	clkcount clkcount1 (.load(wload), .clk(clk), .ready(Led1), .sendNew(wsendnew), .reset(btn1));

endmodule
	

		
