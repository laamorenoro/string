`timescale 1ns / 1ps
module top_tarjeta_tb;
	reg clk, btn1;
	wire Led1;
	wire [6:0] Led;

	top_tarjeta uut ( .clk(clk), .btn1(btn1), .Led1(Led1), .Led(Led));
	
	initial begin  // Initialize Inputs
		clk=0;
		btn1=1;
		#500
		btn1=0;
	end	
     
	initial begin 
		forever begin
		#50
		clk=1;
		#50
		clk=0;		
		end
	end 
	
	initial begin: TEST_CASE
		$dumpfile("top_tarjeta_TB.vcd");
		$dumpvars(-1, uut);
		#60000 $finish;
	end

endmodule
