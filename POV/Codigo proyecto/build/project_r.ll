Revision 3
; Created by bitgen P.15xf at Fri May 31 11:09:05 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit    12831 0x00020000    415 Block=F4 Latch=O2 Net=top1/shiftString1/temp<6>
Bit    15071 0x00020000   2655 Block=R4 Latch=O2 Net=btnTemp
Bit  1086383 0x00280200   3087 Block=U9 Latch=I Net=clk_BUFGP/IBUFG
Bit  1436122 0x00340000   2074 Block=SLICE_X44Y29 Latch=YQ Net=top1/shiftString1/temp<62>
Bit  1436125 0x00340000   2077 Block=SLICE_X44Y29 Latch=XQ Net=top1/shiftString1/temp<63>
Bit  1436154 0x00340000   2106 Block=SLICE_X44Y28 Latch=YQ Net=top1/shiftString1/temp<56>
Bit  1436157 0x00340000   2109 Block=SLICE_X44Y28 Latch=XQ Net=top1/shiftString1/temp<57>
Bit  1436186 0x00340000   2138 Block=SLICE_X44Y27 Latch=YQ Net=top1/shiftString1/temp<60>
Bit  1436189 0x00340000   2141 Block=SLICE_X44Y27 Latch=XQ Net=top1/shiftString1/temp<61>
Bit  1436218 0x00340000   2170 Block=SLICE_X44Y26 Latch=YQ Net=top1/shiftString1/temp<70>
Bit  1436221 0x00340000   2173 Block=SLICE_X44Y26 Latch=XQ Net=top1/shiftString1/temp<71>
Bit  1439226 0x00340200   2074 Block=SLICE_X45Y29 Latch=YQ Net=top1/shiftString1/temp<68>
Bit  1439229 0x00340200   2077 Block=SLICE_X45Y29 Latch=XQ Net=top1/shiftString1/temp<69>
Bit  1439258 0x00340200   2106 Block=SLICE_X45Y28 Latch=YQ Net=top1/shiftString1/temp<54>
Bit  1439261 0x00340200   2109 Block=SLICE_X45Y28 Latch=XQ Net=top1/shiftString1/temp<55>
Bit  1439322 0x00340200   2170 Block=SLICE_X45Y26 Latch=YQ Net=top1/shiftString1/temp<74>
Bit  1439325 0x00340200   2173 Block=SLICE_X45Y26 Latch=XQ Net=top1/shiftString1/temp<75>
Bit  1494906 0x00360000   1882 Block=SLICE_X46Y35 Latch=YQ Net=top1/shiftString1/temp<30>
Bit  1494909 0x00360000   1885 Block=SLICE_X46Y35 Latch=XQ Net=top1/shiftString1/temp<31>
Bit  1495034 0x00360000   2010 Block=SLICE_X46Y31 Latch=YQ Net=top1/shiftString1/temp<38>
Bit  1495037 0x00360000   2013 Block=SLICE_X46Y31 Latch=XQ Net=top1/shiftString1/temp<39>
Bit  1495066 0x00360000   2042 Block=SLICE_X46Y30 Latch=YQ Net=top1/shiftString1/temp<42>
Bit  1495069 0x00360000   2045 Block=SLICE_X46Y30 Latch=XQ Net=top1/shiftString1/temp<43>
Bit  1495098 0x00360000   2074 Block=SLICE_X46Y29 Latch=YQ Net=top1/shiftString1/temp<46>
Bit  1495101 0x00360000   2077 Block=SLICE_X46Y29 Latch=XQ Net=top1/shiftString1/temp<47>
Bit  1495130 0x00360000   2106 Block=SLICE_X46Y28 Latch=YQ Net=top1/shiftString1/temp<76>
Bit  1495162 0x00360000   2138 Block=SLICE_X46Y27 Latch=YQ Net=top1/shiftString1/temp<52>
Bit  1495165 0x00360000   2141 Block=SLICE_X46Y27 Latch=XQ Net=top1/shiftString1/temp<53>
Bit  1495194 0x00360000   2170 Block=SLICE_X46Y26 Latch=YQ Net=top1/shiftString1/temp<66>
Bit  1495197 0x00360000   2173 Block=SLICE_X46Y26 Latch=XQ Net=top1/shiftString1/temp<67>
Bit  1498138 0x00360200   2010 Block=SLICE_X47Y31 Latch=YQ Net=top1/shiftString1/temp<36>
Bit  1498141 0x00360200   2013 Block=SLICE_X47Y31 Latch=XQ Net=top1/shiftString1/temp<37>
Bit  1498170 0x00360200   2042 Block=SLICE_X47Y30 Latch=YQ Net=top1/shiftString1/temp<48>
Bit  1498173 0x00360200   2045 Block=SLICE_X47Y30 Latch=XQ Net=top1/shiftString1/temp<49>
Bit  1498202 0x00360200   2074 Block=SLICE_X47Y29 Latch=YQ Net=top1/shiftString1/temp<44>
Bit  1498205 0x00360200   2077 Block=SLICE_X47Y29 Latch=XQ Net=top1/shiftString1/temp<45>
Bit  1498234 0x00360200   2106 Block=SLICE_X47Y28 Latch=YQ Net=top1/shiftString1/temp<50>
Bit  1498237 0x00360200   2109 Block=SLICE_X47Y28 Latch=XQ Net=top1/shiftString1/temp<51>
Bit  1498266 0x00360200   2138 Block=SLICE_X47Y27 Latch=YQ Net=top1/shiftString1/temp<64>
Bit  1498269 0x00360200   2141 Block=SLICE_X47Y27 Latch=XQ Net=top1/shiftString1/temp<65>
Bit  1498298 0x00360200   2170 Block=SLICE_X47Y26 Latch=YQ Net=top1/shiftString1/temp<58>
Bit  1498301 0x00360200   2173 Block=SLICE_X47Y26 Latch=XQ Net=top1/shiftString1/temp<59>
Bit  1553818 0x00380000   1818 Block=SLICE_X48Y37 Latch=YQ Net=top1/shiftString1/temp<20>
Bit  1553821 0x00380000   1821 Block=SLICE_X48Y37 Latch=XQ Net=top1/shiftString1/temp<21>
Bit  1553914 0x00380000   1914 Block=SLICE_X48Y34 Latch=YQ Net=top1/shiftString1/temp<28>
Bit  1553917 0x00380000   1917 Block=SLICE_X48Y34 Latch=XQ Net=top1/shiftString1/temp<29>
Bit  1553946 0x00380000   1946 Block=SLICE_X48Y33 Latch=YQ Net=top1/shiftString1/temp<40>
Bit  1553949 0x00380000   1949 Block=SLICE_X48Y33 Latch=XQ Net=top1/shiftString1/temp<41>
Bit  1553978 0x00380000   1978 Block=SLICE_X48Y32 Latch=YQ Net=top1/shiftString1/temp<34>
Bit  1553981 0x00380000   1981 Block=SLICE_X48Y32 Latch=XQ Net=top1/shiftString1/temp<35>
Bit  1554138 0x00380000   2138 Block=SLICE_X48Y27 Latch=YQ Net=top1/shiftString1/temp<72>
Bit  1554141 0x00380000   2141 Block=SLICE_X48Y27 Latch=XQ Net=top1/shiftString1/temp<73>
Bit  1557018 0x00380200   1914 Block=SLICE_X49Y34 Latch=YQ Net=top1/shiftString1/temp<26>
Bit  1557021 0x00380200   1917 Block=SLICE_X49Y34 Latch=XQ Net=top1/shiftString1/temp<27>
Bit  1557082 0x00380200   1978 Block=SLICE_X49Y32 Latch=YQ Net=top1/shiftString1/temp<32>
Bit  1557085 0x00380200   1981 Block=SLICE_X49Y32 Latch=XQ Net=top1/shiftString1/temp<33>
Bit  1615866 0x003a0200   1786 Block=SLICE_X50Y38 Latch=YQ Net=top1/shiftString1/temp<14>
Bit  1615869 0x003a0200   1789 Block=SLICE_X50Y38 Latch=XQ Net=top1/shiftString1/temp<15>
Bit  1615930 0x003a0200   1850 Block=SLICE_X50Y36 Latch=YQ Net=top1/shiftString1/temp<12>
Bit  1615933 0x003a0200   1853 Block=SLICE_X50Y36 Latch=XQ Net=top1/shiftString1/temp<13>
Bit  1615962 0x003a0200   1882 Block=SLICE_X50Y35 Latch=YQ Net=top1/shiftString1/temp<24>
Bit  1615965 0x003a0200   1885 Block=SLICE_X50Y35 Latch=XQ Net=top1/shiftString1/temp<25>
Bit  1618938 0x003a0400   1754 Block=SLICE_X51Y39 Latch=YQ Net=top1/shiftString1/temp<22>
Bit  1618941 0x003a0400   1757 Block=SLICE_X51Y39 Latch=XQ Net=top1/shiftString1/temp<23>
Bit  1618970 0x003a0400   1786 Block=SLICE_X51Y38 Latch=YQ Net=top1/shiftString1/temp<16>
Bit  1618973 0x003a0400   1789 Block=SLICE_X51Y38 Latch=XQ Net=top1/shiftString1/temp<17>
Bit  1619002 0x003a0400   1818 Block=SLICE_X51Y37 Latch=YQ Net=top1/shiftString1/temp<18>
Bit  1619005 0x003a0400   1821 Block=SLICE_X51Y37 Latch=XQ Net=top1/shiftString1/temp<19>
Bit  1619034 0x003a0400   1850 Block=SLICE_X51Y36 Latch=YQ Net=top1/shiftString1/temp<6>
Bit  1619037 0x003a0400   1853 Block=SLICE_X51Y36 Latch=XQ Net=top1/shiftString1/temp<7>
Bit  1733786 0x02002400   1754 Block=SLICE_X54Y39 Latch=YQ Net=top1/shiftString1/temp<2>
Bit  1733789 0x02002400   1757 Block=SLICE_X54Y39 Latch=XQ Net=top1/shiftString1/temp<3>
Bit  1733818 0x02002400   1786 Block=SLICE_X54Y38 Latch=YQ Net=top1/shiftString1/temp<0>
Bit  1733821 0x02002400   1789 Block=SLICE_X54Y38 Latch=XQ Net=top1/shiftString1/temp<1>
Bit  1733850 0x02002400   1818 Block=SLICE_X54Y37 Latch=YQ Net=top1/shiftString1/temp<4>
Bit  1733853 0x02002400   1821 Block=SLICE_X54Y37 Latch=XQ Net=top1/shiftString1/temp<5>
Bit  1734010 0x02002400   1978 Block=SLICE_X54Y32 Latch=YQ Net=top1/control1/add
Bit  1736890 0x02002600   1754 Block=SLICE_X55Y39 Latch=YQ Net=top1/shiftString1/temp<8>
Bit  1736893 0x02002600   1757 Block=SLICE_X55Y39 Latch=XQ Net=top1/shiftString1/temp<9>
Bit  1736922 0x02002600   1786 Block=SLICE_X55Y38 Latch=YQ Net=top1/shiftString1/temp<10>
Bit  1736925 0x02002600   1789 Block=SLICE_X55Y38 Latch=XQ Net=top1/shiftString1/temp<11>
Bit  1736954 0x02002600   1818 Block=SLICE_X55Y37 Latch=YQ Net=top1/control1/addEnhe
Bit  2087773 0x02027000   1885 Block=SLICE_X64Y35 Latch=XQ Net=top1/counter1/temp<3>
Bit  2087802 0x02027000   1914 Block=SLICE_X64Y34 Latch=YQ Net=top1/counter1/temp<1>
Bit  2087805 0x02027000   1917 Block=SLICE_X64Y34 Latch=XQ Net=top1/counter1/temp<0>
Bit  2087837 0x02027000   1949 Block=SLICE_X64Y33 Latch=XQ Net=top1/control1/next_state<7>
Bit  2087901 0x02027000   2013 Block=SLICE_X64Y31 Latch=XQ Net=top1/control1/next_state<2>
Bit  2087930 0x02027000   2042 Block=SLICE_X64Y30 Latch=YQ Net=char_1
Bit  2088061 0x02027000   2173 Block=SLICE_X64Y26 Latch=XQ Net=top1/control1/enter
Bit  2090906 0x02027200   1914 Block=SLICE_X65Y34 Latch=YQ Net=top1/counter1/temp<2>
Bit  2090938 0x02027200   1946 Block=SLICE_X65Y33 Latch=YQ Net=top1/control1/delete
Bit  2091002 0x02027200   2010 Block=SLICE_X65Y31 Latch=YQ Net=top1/control1/next_state<1>
Bit  2091037 0x02027200   2045 Block=SLICE_X65Y30 Latch=XQ Net=top1/control1/next_state<6>
Bit  2091066 0x02027200   2074 Block=SLICE_X65Y29 Latch=YQ Net=top1/control1/next_state<5>
Bit  2091133 0x02027200   2141 Block=SLICE_X65Y27 Latch=XQ Net=char_5
Bit  2091197 0x02027200   2205 Block=SLICE_X65Y25 Latch=XQ Net=char_4
Bit  2146845 0x02029600   1981 Block=SLICE_X66Y32 Latch=XQ Net=top1/control1/virgul
Bit  2146973 0x02029600   2109 Block=SLICE_X66Y28 Latch=XQ Net=char_2
Bit  2147002 0x02029600   2138 Block=SLICE_X66Y27 Latch=YQ Net=char_6
Bit  2147037 0x02029600   2173 Block=SLICE_X66Y26 Latch=XQ Net=char_3
Bit  2149914 0x04000000   1946 Block=SLICE_X67Y33 Latch=YQ Net=top1/control1/load
Bit  2149946 0x04000000   1978 Block=SLICE_X67Y32 Latch=YQ Net=top1/control1/next_state<0>
Bit  2149978 0x04000000   2010 Block=SLICE_X67Y31 Latch=YQ Net=top1/control1/next_state<3>
Bit  2149981 0x04000000   2013 Block=SLICE_X67Y31 Latch=XQ Net=top1/control1/next_state<4>
Bit  2150010 0x04000000   2042 Block=SLICE_X67Y30 Latch=YQ Net=new_OBUF
Bit  2150109 0x04000000   2141 Block=SLICE_X67Y27 Latch=XQ Net=char_0
Bit  2204144 0x04002400    304 Block=D16 Latch=O2 Net=btn1Temp
Bit  2204255 0x04002400    415 Block=E17 Latch=O2 Net=top1/shiftString1/temp<4>
Bit  2205232 0x04002400   1392 Block=J14 Latch=O2 Net=top1/shiftString1/temp<0>
Bit  2205255 0x04002400   1415 Block=J15 Latch=O2 Net=top1/shiftString1/temp<1>
Bit  2205488 0x04002400   1648 Block=K14 Latch=O2 Net=top1/shiftString1/temp<3>
Bit  2205511 0x04002400   1671 Block=K15 Latch=O2 Net=top1/shiftString1/temp<2>
Bit  2205639 0x04002400   1799 Block=K13 Latch=O2 Net=btnTemp
Bit  2205744 0x04002400   1904 Block=L17 Latch=O2 Net=charTemp<5>1
Bit  2205872 0x04002400   2032 Block=L15 Latch=O2 Net=charTemp<0>1
Bit  2205895 0x04002400   2055 Block=L16 Latch=O2 Net=charTemp<1>20
Bit  2206000 0x04002400   2160 Block=M18 Latch=O2 Net=charTemp<3>1
Bit  2206023 0x04002400   2183 Block=N18 Latch=O2 Net=charTemp<4>1
Bit  2206128 0x04002400   2288 Block=M16 Latch=O2 Net=charTemp<2>35
Bit  2206151 0x04002400   2311 Block=M15 Latch=O2 Net=charTemp<6>1
Bit  2206495 0x04002400   2655 Block=P15 Latch=O2 Net=top1/shiftString1/temp<5>
Bit  2207040 0x04020000     96 Block=B18 Latch=IQ1 Net=btnTemp
Bit  2207296 0x04020000    352 Block=D18 Latch=IQ1 Net=btn1Temp
Bit  2208000 0x04020000   1056 Block=G18 Latch=IQ1 Net=swTemp<0>
Bit  2208256 0x04020000   1312 Block=H18 Latch=IQ1 Net=swTemp<1>
Bit  2208512 0x04020000   1568 Block=K18 Latch=IQ1 Net=swTemp<2>
Bit  2208768 0x04020000   1824 Block=K17 Latch=IQ1 Net=swTemp<3>
Bit  2209024 0x04020000   2080 Block=L13 Latch=IQ1 Net=swTemp<5>
Bit  2209280 0x04020000   2336 Block=L14 Latch=IQ1 Net=swTemp<4>
Bit  2209536 0x04020000   2592 Block=N17 Latch=IQ1 Net=swTemp<6>
Bit  2209728 0x04020000   2784 Block=R17 Latch=IQ1 Net=swTemp<7>
Bit  2210407 0x04020200    359 Block=D18 Latch=I Net=btn1_IBUF
