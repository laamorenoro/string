module control (clk, cEnter, cVirgul, cEne, cDelete, cCounter, cNum, new, reset, complete, enter, delete, add, load, addEnhe, change, virgul);

input clk;
input cEnter;
input cVirgul;
input cEne;
input cDelete;
input cCounter;
input new;
input reset;
input cNum;
output reg complete;
output reg enter;
output reg delete;
output reg add;
output reg load;
output reg addEnhe;
output reg change;
output reg virgul;


reg [2:0] current_state;
reg [2:0] next_state;

parameter init=3'b000, new_data=3'b001, Add=3'b010, Delete=3'b011, virgulilla=3'b100, AddEnhe=3'b101, Enter=3'b110, effect=3'b111;

initial begin
		current_state<=init;
	end

//or cEnter or cVirgul or cEne or cDelete or cCounter or cNum or new
always @( negedge clk )
	begin
	case (current_state)
		init: begin
			complete=1'b0;
			enter=1'b0;
			delete=1'b0;
			add=1'b0;
			load=1'b1;
			addEnhe=1'b0;
			change=1'b0;
			virgul=1'b0;			
			if (reset==1'b1) next_state<=init;
			else next_state<=new_data;
		end	

		new_data: begin
			complete=1'b0;
			//enter=1'b0;
			delete=1'b0;
			add=1'b0;
			load=1'b0;
			addEnhe=1'b0;
			change=1'b0;		
		if (reset==1'b1) next_state<=init;
			else if((
new==1'b0 | cCounter==1'b1) | (cNum==1'b0 & enter==1'b1)) next_state<=new_data;
				else if (cVirgul==1'b1) next_state<=virgulilla;
					else if(cEnter==1'b1) next_state<=Enter;
			     			else if((cDelete==1'b1 | (cEne==1'b1 & virgul==1'b1))) next_state<=Delete;
							 else if (cNum==1'b1 & enter==1'b1) next_state<=effect;
					
				   				else next_state<=Add;
		end		
		
		Add: begin 
			complete=1'b0;
			enter=1'b0;
			delete=1'b0;
			add=1'b1;
			load=1'b0;
			addEnhe=1'b0;
			change=1'b1;
			virgul=1'b0;
			if(reset) next_state<=init;
			else next_state<=new_data;
		end

		Delete: begin
			   complete=1'b0;
			   enter=1'b0;
			   delete=1'b1;
			   add=1'b0;
			   load=1'b0;
			   addEnhe=1'b0;
			   change=1'b1;
		if (reset==1'b1) next_state<=init;
		else if (cEne==1'b1 & virgul==1) next_state<=AddEnhe;
			else begin next_state<=new_data;
				virgul=1'b0;	
			end
		end
		
		virgulilla: begin 
				complete=1'b0;
				enter=1'b0;
				delete=1'b0;
				add=1'b1;
				load=1'b0;
				addEnhe=1'b0;
				change=1'b1;		
				virgul=1'b1;
				if (reset==1'b1) next_state<=init;
					else next_state<=new_data;
		end

		AddEnhe: begin    				
			complete=1'b0;			
			enter=1'b0;
			delete=1'b0;
			add=1'b1;
			load=1'b0;
			addEnhe=1'b1;
			change=1'b1;		
			virgul=1'b0;
			if (reset==1'b1) next_state<=init;
				else next_state<=new_data;
		end
		
		Enter: begin 
			complete=1'b0;
			enter=1'b1;
			delete=1'b0;
			add=1'b0;
			load=1'b0;
			addEnhe=1'b0;
			change=1'b1;
			if (reset==1'b1) next_state<=init;
				else next_state<=new_data;
		end

		effect: begin 
			complete=1'b1;
			enter=1'b0;
			delete=1'b0;
			add=1'b1;
			load=1'b0;
			addEnhe=1'b0;
			change=1'b1;
			if (reset==1'b1) next_state<=init;
				else next_state<=init;
		end		
		
		default: begin
			complete=1'b0;
			enter=1'b0;
			delete=1'b0;
			add=1'b0;
			load=1'b0;
			addEnhe=1'b0;
			change=1'b0;
			if (reset==1'b1) next_state<=init;
				else next_state<=new_data;
		end

		
	endcase		
	end 


	always @(next_state) current_state<=next_state;

endmodule		
						
				
