Revision 3
; Created by bitgen P.15xf at Thu May 23 08:25:49 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit  1083336 0x00280200     40 Block=P89 Latch=I Net=clk_BUFGP/IBUFG
Bit  2085959 0x02027000     71 Block=P78 Latch=O2 Net=next_state<0>
Bit  2146365 0x02029600   1501 Block=SLICE_X66Y47 Latch=XQ Net=virgul_OBUF
Bit  2146397 0x02029600   1533 Block=SLICE_X66Y46 Latch=XQ Net=next_state<0>
Bit  2146429 0x02029600   1565 Block=SLICE_X66Y45 Latch=XQ Net=next_state<7>
Bit  2146461 0x02029600   1597 Block=SLICE_X66Y44 Latch=XQ Net=next_state<3>
Bit  2146490 0x02029600   1626 Block=SLICE_X66Y43 Latch=YQ Net=next_state<4>
Bit  2146493 0x02029600   1629 Block=SLICE_X66Y43 Latch=XQ Net=next_state<5>
Bit  2146525 0x02029600   1661 Block=SLICE_X66Y42 Latch=XQ Net=next_state<1>
Bit  2149562 0x04000000   1594 Block=SLICE_X67Y44 Latch=YQ Net=next_state<2>
Bit  2149594 0x04000000   1626 Block=SLICE_X67Y43 Latch=YQ Net=next_state<6>
Bit  2149626 0x04000000   1658 Block=SLICE_X67Y42 Latch=YQ Net=enter_OBUF
Bit  2205104 0x04002400   1264 Block=P71 Latch=O2 Net=next_state<3>
Bit  2205232 0x04002400   1392 Block=P68 Latch=O2 Net=next_state<5>
Bit  2205255 0x04002400   1415 Block=P67 Latch=O2 Net=next_state<7>
Bit  2205360 0x04002400   1520 Block=P66 Latch=O2 Net=virgul_mux00001
Bit  2205383 0x04002400   1543 Block=P65 Latch=O2 Net=add_mux00001
Bit  2205488 0x04002400   1648 Block=P63 Latch=O2 Net=change_mux00001
Bit  2205511 0x04002400   1671 Block=P62 Latch=O2 Net=enter_mux00001
Bit  2211343 0x04020200   1295 Block=P70 Latch=I Net=reset_IBUF
Bit  2211367 0x04020200   1319 Block=P69 Latch=I Net=cEnter_IBUF
Bit  2211816 0x04020200   1768 Block=P61 Latch=I Net=cVirgul_IBUF
Bit  2211855 0x04020200   1807 Block=P60 Latch=I Net=cDelete_IBUF
Bit  2212072 0x04020200   2024 Block=P58 Latch=I Net=cNum_IBUF
Bit  2212111 0x04020200   2063 Block=P57 Latch=I Net=cEne_IBUF
Bit  2213032 0x04020200   2984 Block=P54 Latch=I Net=cCounter_IBUF
Bit  2213071 0x04020200   3023 Block=P53 Latch=I Net=new_IBUF
