Revision 3
; Created by bitgen P.15xf at Mon May 20 09:50:28 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit  1083336 0x00280200     40 Block=P89 Latch=I Net=clk_BUFGP/IBUFG
Bit  1083375 0x00280200     79 Block=P88 Latch=I Net=delete_IBUF
Bit  1139216 0x002a0000     48 Block=P86 Latch=O2 Net=temp<0>
Bit  1139239 0x002a0000     71 Block=P85 Latch=O2 Net=Result<3>
Bit  1257168 0x002e0000     48 Block=P84 Latch=O2 Net=Result<1>
Bit  1257191 0x002e0000     71 Block=P83 Latch=O2 Net=Result<2>
Bit  1375162 0x00320000     90 Block=SLICE_X42Y91 Latch=YQ Net=temp<2>
Bit  1375165 0x00320000     93 Block=SLICE_X42Y91 Latch=XQ Net=temp<3>
Bit  1375194 0x00320000    122 Block=SLICE_X42Y90 Latch=YQ Net=temp<1>
Bit  1375197 0x00320000    125 Block=SLICE_X42Y90 Latch=XQ Net=temp<0>
