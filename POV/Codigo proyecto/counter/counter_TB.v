`timescale 1ns / 1ps
module counter_tb;
	reg delete,add;
	wire [7:0] counter;
	reg clk;
	

	counter uut ( .add(add), .delete(delete), .clk(clk), .counter(counter));
	
	initial begin  // Initialize Inputs
		clk=0;
		delete=0;
		add=0;
	end	
     
	initial begin 
		forever begin		
		#220
		add=1;
		#100
		add=0;
		delete=1;
		#80
		delete=0;
		end
	end 

	initial begin 
		forever begin
		#20;
		clk=1;
		#20
		clk=0;		
		end
	end 
	
	initial begin: TEST_CASE
		$dumpfile("counter_TB.vcd");
		$dumpvars(-1, uut);
		#2000 $finish;
	end

endmodule
