module final (clk, string, leds);

	input [76:0] string;
	output reg [6:0] leds;
	input clk;
	reg [6:0] ledsTemp;

	always @(string)begin
		
		ledsTemp<=string[6:0];
		
	end
	
	always @(negedge clk) leds<=ledsTemp;
endmodule


			

