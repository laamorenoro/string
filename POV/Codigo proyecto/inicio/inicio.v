module inicio (sw, btn, btn1, reset, char, new);

	input [7:0] sw;
	input btn, btn1;
	output reg [6:0] char;
	output reg new;

	always @(btn)begin
		if (btn1==0) reset=1;
		else reset=0;
		if(btn==0) new=1;
		else new=0;
		
		case (sw)
			
			7'b01111111:char=7'b1111011;
			7'b10111111:char=7'b0111111;
			7'b11011111:char=7'b0000111;
			7'b11101111:char=7'b0111011;
			7'b11110111:char=7'b0001000;
			7'b11111011:char=7'b1000110;
			7'b11111101:char=7'b0010000;
			7'b01111110:char=7'b0100110;
		endcase
	end

endmodule


			


