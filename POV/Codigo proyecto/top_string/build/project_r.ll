Revision 3
; Created by bitgen P.15xf at Wed May 29 10:16:32 2013
; Bit lines have the following form:
; <offset> <frame address> <frame offset> <information>
; <information> may be zero or more <kw>=<value> pairs
; Block=<blockname     specifies the block associated with this
;                      memory cell.
;
; Latch=<name>         specifies the latch associated with this memory cell.
;
; Net=<netname>        specifies the user net associated with this
;                      memory cell.
;
; COMPARE=[YES | NO]   specifies whether or not it is appropriate
;                      to compare this bit position between a
;                      "program" and a "readback" bitstream.
;                      If not present the default is NO.
;
; Ram=<ram id>:<bit>   This is used in cases where a CLB function
; Rom=<ram id>:<bit>   generator is used as RAM (or ROM).  <Ram id>
;                      will be either 'F', 'G', or 'M', indicating
;                      that it is part of a single F or G function
;                      generator used as RAM, or as a single RAM
;                      (or ROM) built from both F and G.  <Bit> is
;                      a decimal number.
;
; Info lines have the following form:
; Info <name>=<value>  specifies a bit associated with the LCA
;                      configuration options, and the value of
;                      that bit.  The names of these bits may have
;                      special meaning to software reading the .ll file.
;
Info STARTSEL0=1
Bit   195631 0x000a0000     79 Block=A4 Latch=I Net=new_IBUF
Bit   254567 0x000c0000     39 Block=C4 Latch=I Net=char_6_IBUF
Bit   313544 0x000e0000     40 Block=D5 Latch=I Net=btn1_IBUF
Bit   313583 0x000e0000     79 Block=C5 Latch=I Net=char_2_IBUF
Bit   372520 0x00100000     40 Block=B5 Latch=I Net=char_5_IBUF
Bit   372559 0x00100000     79 Block=A5 Latch=I Net=char_3_IBUF
Bit   484272 0x00122200     48 Block=A6 Latch=O2 Net=control1/next_state<7>
Bit   490511 0x00140000     79 Block=B6 Latch=I Net=char_4_IBUF
Bit   549447 0x00160000     39 Block=A7 Latch=I Net=char_1_IBUF
Bit   602247 0x00162200     71 Block=F7 Latch=O2 Net=control1/change_mux00001
Bit   602397 0x00162200    221 Block=SLICE_X16Y87 Latch=XQ Net=control1/enter
Bit   608424 0x00180000     40 Block=E7 Latch=I Net=char_0_IBUF
Bit   664304 0x00182400     48 Block=D7 Latch=O2 Net=shiftString1/temp_mux0000<76>
Bit   664327 0x00182400     71 Block=C7 Latch=O2 Net=shiftString1/temp_mux0000<73>
Bit   664509 0x00182400    253 Block=SLICE_X18Y86 Latch=XQ Net=control1/next_state<1>
Bit   664570 0x00182400    314 Block=SLICE_X18Y84 Latch=YQ Net=control1/next_state<6>
Bit   667613 0x001a0000    253 Block=SLICE_X19Y86 Latch=XQ Net=control1/next_state<0>
Bit   667677 0x001a0000    317 Block=SLICE_X19Y84 Latch=XQ Net=control1/next_state<7>
Bit   723263 0x001a2400     31 Block=A8 Latch=O2 Net=shiftString1/temp_mux0000<60>
Bit   723549 0x001a2400    317 Block=SLICE_X20Y84 Latch=XQ Net=control1/next_state<3>
Bit   726650 0x001c0000    314 Block=SLICE_X21Y84 Latch=YQ Net=control1/next_state<2>
Bit   782256 0x001c2400     48 Block=F8 Latch=O2 Net=shiftString1/temp_mux0000<71>
Bit   782279 0x001c2400     71 Block=E8 Latch=O2 Net=shiftString1/temp_mux0000<55>
Bit   782554 0x001c2400    346 Block=SLICE_X22Y83 Latch=YQ Net=control1/virgul
Bit   785626 0x001e0000    314 Block=SLICE_X23Y84 Latch=YQ Net=control1/next_state<4>
Bit   785690 0x001e0000    378 Block=SLICE_X23Y82 Latch=YQ Net=control1/next_state<5>
Bit   900208 0x00202400     48 Block=F9 Latch=O2 Net=shiftString1/temp_mux0000<48>
Bit   900231 0x00202400     71 Block=E9 Latch=O2 Net=shiftString1/temp_mux0000<63>
Bit   959167 0x00222400     31 Block=G9 Latch=O2 Net=shiftString1/temp_mux0000<74>
Bit  1018160 0x00242400     48 Block=D9 Latch=O2 Net=shiftString1/temp_mux0000<58>
Bit  1018183 0x00242400     71 Block=C9 Latch=O2 Net=shiftString1/temp_mux0000<72>
Bit  1083336 0x00280200     40 Block=B9 Latch=I Net=clk_BUFGP/IBUFG
Bit  1139216 0x002a0000     48 Block=A10 Latch=O2 Net=shiftString1/temp_mux0000<66>
Bit  1139239 0x002a0000     71 Block=B10 Latch=O2 Net=shiftString1/temp_mux0000<75>
Bit  1139645 0x002a0000    477 Block=SLICE_X34Y79 Latch=XQ Net=counter1/temp<3>
Bit  1139674 0x002a0000    506 Block=SLICE_X34Y78 Latch=YQ Net=counter1/temp<1>
Bit  1139677 0x002a0000    509 Block=SLICE_X34Y78 Latch=XQ Net=counter1/temp<0>
Bit  1142778 0x002a0200    506 Block=SLICE_X35Y78 Latch=YQ Net=counter1/temp<2>
Bit  1198175 0x002c0000     31 Block=B11 Latch=O2 Net=shiftString1/temp_mux0000<70>
Bit  1257168 0x002e0000     48 Block=E10 Latch=O2 Net=shiftString1/temp_mux0000<64>
Bit  1257191 0x002e0000     71 Block=D10 Latch=O2 Net=shiftString1/temp_mux0000<54>
Bit  1375120 0x00320000     48 Block=D11 Latch=O2 Net=shiftString1/temp_mux0000<68>
Bit  1375143 0x00320000     71 Block=C11 Latch=O2 Net=shiftString1/temp_mux0000<69>
Bit  1434079 0x00340000     31 Block=A11 Latch=O2 Net=shiftString1/temp_mux0000<61>
Bit  1493072 0x00360000     48 Block=F11 Latch=O2 Net=shiftString1/temp_mux0000<67>
Bit  1493095 0x00360000     71 Block=E11 Latch=O2 Net=shiftString1/temp_mux0000<65>
Bit  1493434 0x00360000    410 Block=SLICE_X46Y81 Latch=YQ Net=shiftString1/temp<72>
Bit  1493437 0x00360000    413 Block=SLICE_X46Y81 Latch=XQ Net=shiftString1/temp<73>
Bit  1493466 0x00360000    442 Block=SLICE_X46Y80 Latch=YQ Net=shiftString1/temp<66>
Bit  1493469 0x00360000    445 Block=SLICE_X46Y80 Latch=XQ Net=shiftString1/temp<67>
Bit  1496538 0x00360200    410 Block=SLICE_X47Y81 Latch=YQ Net=shiftString1/temp<74>
Bit  1496541 0x00360200    413 Block=SLICE_X47Y81 Latch=XQ Net=shiftString1/temp<75>
Bit  1496570 0x00360200    442 Block=SLICE_X47Y80 Latch=YQ Net=shiftString1/temp<64>
Bit  1496573 0x00360200    445 Block=SLICE_X47Y80 Latch=XQ Net=shiftString1/temp<65>
Bit  1496602 0x00360200    474 Block=SLICE_X47Y79 Latch=YQ Net=shiftString1/temp<60>
Bit  1496605 0x00360200    477 Block=SLICE_X47Y79 Latch=XQ Net=shiftString1/temp<61>
Bit  1496634 0x00360200    506 Block=SLICE_X47Y78 Latch=YQ Net=shiftString1/temp<68>
Bit  1496637 0x00360200    509 Block=SLICE_X47Y78 Latch=XQ Net=shiftString1/temp<69>
Bit  1552410 0x00380000    410 Block=SLICE_X48Y81 Latch=YQ Net=shiftString1/temp<70>
Bit  1552413 0x00380000    413 Block=SLICE_X48Y81 Latch=XQ Net=shiftString1/temp<71>
Bit  1555056 0x00380000   3056 Block=V13 Latch=O2 Net=shiftString1/temp_mux0000<6>
Bit  1555079 0x00380000   3079 Block=V12 Latch=O2 Net=shiftString1/temp_mux0000<29>
Bit  1614128 0x003a0200     48 Block=E12 Latch=O2 Net=shiftString1/temp_mux0000<59>
Bit  1614151 0x003a0200     71 Block=F12 Latch=O2 Net=shiftString1/temp_mux0000<1>
Bit  1614490 0x003a0200    410 Block=SLICE_X50Y81 Latch=YQ Net=shiftString1/temp<58>
Bit  1614493 0x003a0200    413 Block=SLICE_X50Y81 Latch=XQ Net=shiftString1/temp<59>
Bit  1614586 0x003a0200    506 Block=SLICE_X50Y78 Latch=YQ Net=shiftString1/temp<62>
Bit  1614589 0x003a0200    509 Block=SLICE_X50Y78 Latch=XQ Net=shiftString1/temp<63>
Bit  1614938 0x003a0200    858 Block=SLICE_X50Y67 Latch=YQ Net=control1/add
Bit  1614970 0x003a0200    890 Block=SLICE_X50Y66 Latch=YQ Net=shiftString1/temp<76>
Bit  1615034 0x003a0200    954 Block=SLICE_X50Y64 Latch=YQ Net=control1/addEnhe
Bit  1617136 0x003a0200   3056 Block=R12 Latch=O2 Net=shiftString1/temp_mux0000<0>
Bit  1617690 0x003a0400    506 Block=SLICE_X51Y78 Latch=YQ Net=shiftString1/temp<54>
Bit  1617693 0x003a0400    509 Block=SLICE_X51Y78 Latch=XQ Net=shiftString1/temp<55>
Bit  1618074 0x003a0400    890 Block=SLICE_X51Y66 Latch=YQ Net=control1/delete
Bit  1618234 0x003a0400   1050 Block=SLICE_X51Y61 Latch=YQ Net=shiftString1/temp<0>
Bit  1618237 0x003a0400   1053 Block=SLICE_X51Y61 Latch=XQ Net=shiftString1/temp<1>
Bit  1674138 0x003c0200   1082 Block=SLICE_X52Y60 Latch=YQ Net=shiftString1/temp<28>
Bit  1674141 0x003c0200   1085 Block=SLICE_X52Y60 Latch=XQ Net=shiftString1/temp<29>
Bit  1674202 0x003c0200   1146 Block=SLICE_X52Y58 Latch=YQ Net=shiftString1/temp<22>
Bit  1674205 0x003c0200   1149 Block=SLICE_X52Y58 Latch=XQ Net=shiftString1/temp<23>
Bit  1677210 0x02000000   1050 Block=SLICE_X53Y61 Latch=YQ Net=shiftString1/temp<14>
Bit  1677213 0x02000000   1053 Block=SLICE_X53Y61 Latch=XQ Net=shiftString1/temp<15>
Bit  1677242 0x02000000   1082 Block=SLICE_X53Y60 Latch=YQ Net=shiftString1/temp<6>
Bit  1677245 0x02000000   1085 Block=SLICE_X53Y60 Latch=XQ Net=shiftString1/temp<7>
Bit  1732080 0x02002400     48 Block=B13 Latch=O2 Net=shiftString1/temp_mux0000<51>
Bit  1732103 0x02002400     71 Block=A13 Latch=O2 Net=shiftString1/temp_mux0000<57>
Bit  1732506 0x02002400    474 Block=SLICE_X54Y79 Latch=YQ Net=shiftString1/temp<50>
Bit  1732509 0x02002400    477 Block=SLICE_X54Y79 Latch=XQ Net=shiftString1/temp<51>
Bit  1732538 0x02002400    506 Block=SLICE_X54Y78 Latch=YQ Net=shiftString1/temp<56>
Bit  1732541 0x02002400    509 Block=SLICE_X54Y78 Latch=XQ Net=shiftString1/temp<57>
Bit  1733210 0x02002400   1178 Block=SLICE_X54Y57 Latch=YQ Net=shiftString1/temp<16>
Bit  1733213 0x02002400   1181 Block=SLICE_X54Y57 Latch=XQ Net=shiftString1/temp<17>
Bit  1733338 0x02002400   1306 Block=SLICE_X54Y53 Latch=YQ Net=shiftString1/temp<2>
Bit  1733341 0x02002400   1309 Block=SLICE_X54Y53 Latch=XQ Net=shiftString1/temp<3>
Bit  1733754 0x02002400   1722 Block=SLICE_X54Y40 Latch=YQ Net=shiftString1/temp<4>
Bit  1733757 0x02002400   1725 Block=SLICE_X54Y40 Latch=XQ Net=shiftString1/temp<5>
Bit  1735088 0x02002400   3056 Block=R13 Latch=O2 Net=shiftString1/temp_mux0000<14>
Bit  1735610 0x02002600    474 Block=SLICE_X55Y79 Latch=YQ Net=shiftString1/temp<48>
Bit  1735613 0x02002600    477 Block=SLICE_X55Y79 Latch=XQ Net=shiftString1/temp<49>
Bit  1735642 0x02002600    506 Block=SLICE_X55Y78 Latch=YQ Net=shiftString1/temp<52>
Bit  1735645 0x02002600    509 Block=SLICE_X55Y78 Latch=XQ Net=shiftString1/temp<53>
Bit  1736346 0x02002600   1210 Block=SLICE_X55Y56 Latch=YQ Net=shiftString1/temp<24>
Bit  1736349 0x02002600   1213 Block=SLICE_X55Y56 Latch=XQ Net=shiftString1/temp<25>
Bit  1736474 0x02002600   1338 Block=SLICE_X55Y52 Latch=YQ Net=shiftString1/temp<8>
Bit  1736477 0x02002600   1341 Block=SLICE_X55Y52 Latch=XQ Net=shiftString1/temp<9>
Bit  1736826 0x02002600   1690 Block=SLICE_X55Y41 Latch=YQ Net=shiftString1/temp<18>
Bit  1736829 0x02002600   1693 Block=SLICE_X55Y41 Latch=XQ Net=shiftString1/temp<19>
Bit  1736858 0x02002600   1722 Block=SLICE_X55Y40 Latch=YQ Net=shiftString1/temp<10>
Bit  1736861 0x02002600   1725 Block=SLICE_X55Y40 Latch=XQ Net=shiftString1/temp<11>
Bit  1791056 0x02004a00     48 Block=A14 Latch=O2 Net=shiftString1/temp_mux0000<52>
Bit  1791079 0x02004a00     71 Block=B14 Latch=O2 Net=shiftString1/temp_mux0000<53>
Bit  1850015 0x02007000     31 Block=E13 Latch=O2 Net=shiftString1/temp_mux0000<56>
Bit  1909008 0x02009600     48 Block=C14 Latch=O2 Net=shiftString1/temp_mux0000<49>
Bit  1909031 0x02009600     71 Block=D14 Latch=O2 Net=shiftString1/temp_mux0000<62>
Bit  2085936 0x02027000     48 Block=A16 Latch=O2 Net=shiftString1/temp_mux0000<47>
Bit  2085959 0x02027000     71 Block=B16 Latch=O2 Net=shiftString1/temp_mux0000<46>
Bit  2086362 0x02027000    474 Block=SLICE_X64Y79 Latch=YQ Net=shiftString1/temp<46>
Bit  2086365 0x02027000    477 Block=SLICE_X64Y79 Latch=XQ Net=shiftString1/temp<47>
Bit  2086394 0x02027000    506 Block=SLICE_X64Y78 Latch=YQ Net=shiftString1/temp<42>
Bit  2086397 0x02027000    509 Block=SLICE_X64Y78 Latch=XQ Net=shiftString1/temp<43>
Bit  2086938 0x02027000   1050 Block=SLICE_X64Y61 Latch=YQ Net=shiftString1/temp<34>
Bit  2086941 0x02027000   1053 Block=SLICE_X64Y61 Latch=XQ Net=shiftString1/temp<35>
Bit  2089466 0x02027200    474 Block=SLICE_X65Y79 Latch=YQ Net=shiftString1/temp<38>
Bit  2089469 0x02027200    477 Block=SLICE_X65Y79 Latch=XQ Net=shiftString1/temp<39>
Bit  2089498 0x02027200    506 Block=SLICE_X65Y78 Latch=YQ Net=shiftString1/temp<44>
Bit  2089501 0x02027200    509 Block=SLICE_X65Y78 Latch=XQ Net=shiftString1/temp<45>
Bit  2089722 0x02027200    730 Block=SLICE_X65Y71 Latch=YQ Net=shiftString1/temp<36>
Bit  2089725 0x02027200    733 Block=SLICE_X65Y71 Latch=XQ Net=shiftString1/temp<37>
Bit  2089754 0x02027200    762 Block=SLICE_X65Y70 Latch=YQ Net=shiftString1/temp<30>
Bit  2089757 0x02027200    765 Block=SLICE_X65Y70 Latch=XQ Net=shiftString1/temp<31>
Bit  2090010 0x02027200   1018 Block=SLICE_X65Y62 Latch=YQ Net=shiftString1/temp<40>
Bit  2090013 0x02027200   1021 Block=SLICE_X65Y62 Latch=XQ Net=shiftString1/temp<41>
Bit  2090042 0x02027200   1050 Block=SLICE_X65Y61 Latch=YQ Net=shiftString1/temp<32>
Bit  2090045 0x02027200   1053 Block=SLICE_X65Y61 Latch=XQ Net=shiftString1/temp<33>
Bit  2090362 0x02027200   1370 Block=SLICE_X65Y51 Latch=YQ Net=shiftString1/temp<26>
Bit  2090365 0x02027200   1373 Block=SLICE_X65Y51 Latch=XQ Net=shiftString1/temp<27>
Bit  2090394 0x02027200   1402 Block=SLICE_X65Y50 Latch=YQ Net=shiftString1/temp<20>
Bit  2090397 0x02027200   1405 Block=SLICE_X65Y50 Latch=XQ Net=shiftString1/temp<21>
Bit  2090714 0x02027200   1722 Block=SLICE_X65Y40 Latch=YQ Net=control1/load
Bit  2090842 0x02027200   1850 Block=SLICE_X65Y36 Latch=YQ Net=shiftString1/temp<12>
Bit  2090845 0x02027200   1853 Block=SLICE_X65Y36 Latch=XQ Net=shiftString1/temp<13>
Bit  2204016 0x04002400    176 Block=C17 Latch=O2 Net=shiftString1/temp_mux0000<45>
Bit  2204039 0x04002400    199 Block=C18 Latch=O2 Net=shiftString1/temp_mux0000<44>
Bit  2204144 0x04002400    304 Block=D16 Latch=O2 Net=shiftString1/temp_mux0000<43>
Bit  2204167 0x04002400    327 Block=D17 Latch=O2 Net=shiftString1/temp_mux0000<42>
Bit  2204255 0x04002400    415 Block=E17 Latch=O2 Net=shiftString1/temp_mux0000<38>
Bit  2204336 0x04002400    496 Block=F14 Latch=O2 Net=shiftString1/temp_mux0000<50>
Bit  2204359 0x04002400    519 Block=F15 Latch=O2 Net=shiftString1/temp_mux0000<39>
Bit  2204464 0x04002400    624 Block=G13 Latch=O2 Net=shiftString1/temp_mux0000<30>
Bit  2204487 0x04002400    647 Block=G14 Latch=O2 Net=shiftString1/temp_mux0000<17>
Bit  2204592 0x04002400    752 Block=F17 Latch=O2 Net=shiftString1/temp_mux0000<37>
Bit  2204615 0x04002400    775 Block=F18 Latch=O2 Net=shiftString1/temp_mux0000<36>
Bit  2204720 0x04002400    880 Block=G16 Latch=O2 Net=shiftString1/temp_mux0000<35>
Bit  2204743 0x04002400    903 Block=G15 Latch=O2 Net=shiftString1/temp_mux0000<7>
Bit  2204848 0x04002400   1008 Block=H15 Latch=O2 Net=shiftString1/temp_mux0000<28>
Bit  2204871 0x04002400   1031 Block=H14 Latch=O2 Net=shiftString1/temp_mux0000<34>
Bit  2204976 0x04002400   1136 Block=H17 Latch=O2 Net=shiftString1/temp_mux0000<40>
Bit  2204999 0x04002400   1159 Block=H16 Latch=O2 Net=shiftString1/temp_mux0000<24>
Bit  2205104 0x04002400   1264 Block=J13 Latch=O2 Net=shiftString1/temp_mux0000<23>
Bit  2205127 0x04002400   1287 Block=J12 Latch=O2 Net=shiftString1/temp_mux0000<21>
Bit  2205232 0x04002400   1392 Block=J14 Latch=O2 Net=shiftString1/temp_mux0000<12>
Bit  2205255 0x04002400   1415 Block=J15 Latch=O2 Net=shiftString1/temp_mux0000<10>
Bit  2205360 0x04002400   1520 Block=J16 Latch=O2 Net=shiftString1/temp_mux0000<9>
Bit  2205383 0x04002400   1543 Block=J17 Latch=O2 Net=shiftString1/temp_mux0000<20>
Bit  2205488 0x04002400   1648 Block=K14 Latch=O2 Net=shiftString1/temp_mux0000<15>
Bit  2205511 0x04002400   1671 Block=K15 Latch=O2 Net=shiftString1/temp_mux0000<13>
Bit  2205616 0x04002400   1776 Block=K12 Latch=O2 Net=shiftString1/temp_mux0000<3>
Bit  2205639 0x04002400   1799 Block=K13 Latch=O2 Net=shiftString1/temp_mux0000<31>
Bit  2205744 0x04002400   1904 Block=L17 Latch=O2 Net=shiftString1/temp_mux0000<41>
Bit  2205767 0x04002400   1927 Block=L18 Latch=O2 Net=shiftString1/temp_mux0000<2>
Bit  2205872 0x04002400   2032 Block=L15 Latch=O2 Net=shiftString1/temp_mux0000<27>
Bit  2205895 0x04002400   2055 Block=L16 Latch=O2 Net=shiftString1/temp_mux0000<16>
Bit  2206000 0x04002400   2160 Block=M18 Latch=O2 Net=shiftString1/temp_mux0000<32>
Bit  2206023 0x04002400   2183 Block=N18 Latch=O2 Net=shiftString1/temp_mux0000<8>
Bit  2206128 0x04002400   2288 Block=M16 Latch=O2 Net=shiftString1/temp_mux0000<25>
Bit  2206151 0x04002400   2311 Block=M15 Latch=O2 Net=shiftString1/temp_mux0000<5>
Bit  2206256 0x04002400   2416 Block=P18 Latch=O2 Net=shiftString1/temp_mux0000<18>
Bit  2206279 0x04002400   2439 Block=P17 Latch=O2 Net=shiftString1/temp_mux0000<11>
Bit  2206384 0x04002400   2544 Block=M13 Latch=O2 Net=shiftString1/temp_mux0000<4>
Bit  2206407 0x04002400   2567 Block=M14 Latch=O2 Net=shiftString1/temp_mux0000<19>
Bit  2206495 0x04002400   2655 Block=P15 Latch=O2 Net=shiftString1/temp_mux0000<26>
Bit  2206576 0x04002400   2736 Block=R16 Latch=O2 Net=shiftString1/temp_mux0000<22>
Bit  2206704 0x04002400   2864 Block=T18 Latch=O2 Net=shiftString1/temp_mux0000<33>
