module top_tarjeta(sw, btn, btn1, clk, char, new, reset, Led, Led1);

	input [7:0] sw;
	input btn;
	input btn1, clk;
	output [6:0] Led;
	output reg Led1;	
	
	output reg [6:0] char;
	output reg new;
	wire [76:0] wstring;
	output reg reset;
	wire wcomplete;
	wire wchange;

	reg [7:0] swTemp;
	reg btnTemp, btn1Temp;
	reg newTemp, resetTemp, Led1Temp;
	reg [6:0] charTemp; 	

	always @(posedge clk) begin
		btnTemp<=btn;
		btn1Temp<=btn1;
		swTemp<=sw;
	end

	always @(btnTemp or btn1Temp or swTemp or newTemp)begin
		case (swTemp)
			~(8'b01111111):charTemp=7'b1111011; //Si ó
			~(8'b10111111):charTemp=7'b0111111; //Si virgul
			~(8'b11011111):charTemp=7'b0000111; //Si p
			~(8'b11101111):charTemp=7'b0111011; //Si n
			~(8'b11110111):charTemp=7'b0100110; //Si 2
			~(8'b11111011):charTemp=7'b1000110; //Si 1
			~(8'b11111101):charTemp=7'b0010000; // borrar 
			~(8'b11111110):charTemp=7'b0001000; // enter!!!!!!
		default:charTemp=7'b0000010; //Si Espacio
		endcase
	
		if (btn1Temp==1) resetTemp=1;
		else resetTemp=0;
		if(btnTemp==1) newTemp=1;
		else newTemp=0;
		Led1Temp<=newTemp;
		
	end

	always @(posedge clk) begin
		char<=charTemp;
		new<=newTemp;
		reset<=resetTemp;
		Led1<=Led1Temp;
		end 
		
	top_string top1 (.char(char), .clk(clk), .new(new), .change(wchange), .complete(wcomplete), .btn1(btn1), .string(wstring));
	final final1 (.string(wstring), .leds(Led), .clk(clk));

endmodule
	

		
