`timescale 1ns / 1ps
module top_string_tb;
	reg clk, btn, btn1;
	reg [7:0] sw;
	wire Led1;
	wire [6:0] Led;

	top_tarjeta uut ( .btn(btn), .clk(clk), .btn1(btn1), .sw(sw), .Led1(Led1), .Led(Led));
	
	initial begin  // Initialize Inputs
		clk=0;
		btn=0;
		btn1=1;
		sw=7'b00000000;	
		
	end	
     
	initial begin 
		forever begin		
		#200
		btn=1;
		btn1=0;
		#100
		btn=0;
		#200
		btn=1;
		#100
		btn=0;
		end
	end 
	
	initial begin 
		forever begin		
		#150
		sw=8'b10000000;		//o
		#300
		sw=8'b00100000; //p 
		#300		
		sw=8'b11111111;
		end

	end 

	initial begin 
		forever begin
		#50
		clk=1;
		#50
		clk=0;		
		end
	end 
	
	initial begin: TEST_CASE
		$dumpfile("top_tarjeta_TB.vcd");
		$dumpvars(-1, uut);
		#60000 $finish;
	end

endmodule
